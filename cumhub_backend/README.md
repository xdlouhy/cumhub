# CumHub Backend

Configure server by environment variables, or by creating `.env` file according to `.env.example`.

Run backend server by:
~~~ 
cargo run
 ~~~   
Basic documentation of endpoints is available in Swagger, which is available on:
~~~ 
http://<hostname>:<port>/swagger-ui/
 ~~~  

## IMPORTANT:

schema.rs needs to contain this, as diesel does not handle views well:
~~~
diesel::table! {
    comment_details_view {
        id -> Uuid,
        author_id -> Uuid,
        #[max_length = 255]
        author_username -> Varchar,
        video_id -> Uuid,
        #[max_length = 255]
        content -> Varchar,
        created_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    channel_info {
        id -> Uuid,
        #[max_length = 255]
        username -> Varchar,
        #[max_length = 255]
        description -> Nullable<Varchar>,
        #[max_length = 255]
        profile_picture -> Nullable<Varchar>,
        subscribers -> Int8
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Category;

    video_details_view {
        id -> Uuid,
        author_id -> Uuid,
        #[max_length = 255]
        author_username -> Varchar,
        #[max_length = 255]
        title -> Varchar,
        #[max_length = 255]
        description -> Nullable<Varchar>,
        #[max_length = 255]
        url -> Varchar,
        views -> Int4,
        length -> Int4,
        is_premium -> Bool,
        uploaded_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
        categories -> Array<Category>,
        likes -> Int8,
    }
}
~~~
