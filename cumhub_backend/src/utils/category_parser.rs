use std::collections::HashMap;

// pub fn to_pascal_case(s: &str) -> String {
//     let mut result = String::new();
//     let mut capitalize_next = true;
//     for c in s.chars() {
//         if c == '_' {
//             capitalize_next = true;
//         } else if capitalize_next {
//             result.extend(c.to_uppercase());
//             capitalize_next = false;
//         } else {
//             result.push(c);
//         }
//     }
//     result
// }

pub fn parse_category(category: Option<String>) -> String { 
    let category = match category {
        Some(category) => category,
        None => String::from("Trending")
    };
    
    let mut categories_map: HashMap<String,String> = HashMap::new(        
    );

    categories_map.insert(String::from("lesbian"), String::from("Lesbian"));
    categories_map.insert(String::from("bdsm"), String::from("BDSM"));
    categories_map.insert(String::from("fetish"), String::from("Fetish"));
    categories_map.insert(String::from("czech"), String::from("Czech"));
    categories_map.insert(String::from("feet"), String::from("Foot Fetish"));
    categories_map.insert(String::from("goth"), String::from("Goth"));
    categories_map.insert(String::from("gangbang"), String::from("Gangbang"));
    categories_map.insert(String::from("muscle_mommies"), String::from("Muscle mommies"));

    let parsed_category = categories_map.get(category.to_lowercase().as_str());
   
    match parsed_category {
        Some(category) => category.to_string(),
        _ => String::from("Trending")
    }


}