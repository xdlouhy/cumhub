use actix_web::web::Query;
use std::str::FromStr;

use crate::{
    models::Category,
    repositories::{
        common::Pagination,
        video::models::{SelectVideoManyFilter, SelectVideoManyFilterRaw, SortBy},
    },
};

pub fn map_select_video_many_filter_raw_to_mapper(
    filters: Query<SelectVideoManyFilterRaw>,
) -> SelectVideoManyFilter {
    SelectVideoManyFilter {
        author_ids: filters.author_ids.clone(),
        title_substring: filters.title_substring.clone(),
        category: Category::from_str(filters.category.clone().unwrap_or("Vanilla".to_string()).as_str())
            .ok(),
        length_from: filters.length_from,
        length_to: filters.length_to,
        include_premium: filters.include_premium,
        pagination: Some(Pagination {
            limit: filters.limit,
            offset: filters.offset,
        }),
        sort_by: map_sort_by_string(filters.sort_by.clone().unwrap_or("".to_string())),
    }
}

pub fn map_sort_by_string(sort_string: String) -> Option<SortBy> {
    match sort_string.to_lowercase().as_str() {
        "oldest" => Some(SortBy::Oldest),
        "most_liked" => Some(SortBy::MostLiked),
        "most_disliked" => Some(SortBy::MostDisliked),
        "most_viewed" => Some(SortBy::MostViewed),
        _ => Some(SortBy::Newest),
    }
}

pub fn map_sort_by_to_string(sort_by: SortBy) -> String {
    match sort_by {
        SortBy::Oldest => "oldest".to_string(),
        SortBy::MostLiked => "most_liked".to_string(),
        SortBy::MostDisliked => "most_disliked".to_string(),
        SortBy::MostViewed => "most_viewed".to_string(),
        _ => "newest".to_string(),
    }
}
