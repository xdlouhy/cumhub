use crate::repositories::user::repository::{PgUserRepository, UserRepository};
use crate::templates::{AccessDeniedTemplate, LoginPageContentTemplate, UserInfo};

use anyhow::Error;
use jwt_simple::prelude::*;
use orion::errors::UnknownCryptoError;
use orion::pwhash;
use orion::pwhash::{hash_password_verify, Password, PasswordHash};
use std::ops::Deref;

use actix_web::web::Data;
use actix_web::{
    cookie::Cookie, error::ErrorInternalServerError, post, HttpRequest, HttpResponse,
    Result as ActixResult,
};
use askama::Template;
use chrono::Utc;
use std::sync::Arc;

pub fn redirect_access_denied() -> ActixResult<HttpResponse> {
    let redirect_url = "/access_denied";

    return Ok(HttpResponse::Forbidden()
        .content_type("text/html")
        .append_header(("HX-Push-Url", redirect_url))
        .body("User id does not match auth token!"));
}

pub fn hash_password(password_str: String) -> Result<PasswordHash, UnknownCryptoError> {
    let password = Password::from_slice(password_str.as_bytes())?;
    pwhash::hash_password(&password, 3, 1 << 16)
}

pub fn verify_password(password_hash: String, password_str: String) -> Result<(), UnknownCryptoError> {
    let hash = match PasswordHash::from_encoded(password_hash.as_str()) {
        Ok(password_hash) => password_hash,
        Err(err) => return Err(err),
    };

    let password = match Password::from_slice(password_str.as_bytes()) {
        Ok(pass) => pass,
        Err(err) => return Err(err),
    };

    hash_password_verify(&hash, &password)
}

pub fn generate_jwt_key() -> HS256Key {
    HS256Key::generate()
}

pub fn create_jwt(user_info: UserInfo, key: &HS256Key) -> Result<String, Error> {
    let claims = Claims::with_custom_claims(user_info, Duration::from_hours(1));
    let token = key.authenticate(claims)?;
    Ok(token)
}

pub struct AuthHandler<'a> {
    pub cookies: Vec<Cookie<'a>>,
    key: &'a Arc<HS256Key>,
    user_repo: &'a Data<PgUserRepository>,
}

impl<'a> AuthHandler<'a> {
    pub fn new(
        cookies: Vec<Cookie<'a>>,
        key: &'a Arc<HS256Key>,
        user_repo: &'a Data<PgUserRepository>,
    ) -> AuthHandler<'a> {
        AuthHandler {
            cookies,
            key,
            user_repo,
        }
    }

    pub fn user_logged_in(&self) -> bool {
        self.get_user_info_from_token().is_some()
    }

    pub fn is_user_premium(&self) -> bool {
        self.get_user_info_from_token()
            .is_some_and(|u| u.premium_expiration.is_some_and(|date| date > Utc::now()))
    }

    pub fn get_user_info_from_token(&self) -> Option<UserInfo> {
        match self.get_token_from_cookie() {
            AuthState::LoggedIn(token) => {

                let claims = match self.key.verify_token::<UserInfo>(&token, None) {
                    Ok(x) => x,
                    Err(_) => {
                        return None;
                    }
                };
                let now = Some(Clock::now_since_epoch());

                if as_ticks(claims.expires_at) < as_ticks(now) {
                    println!("Token expired already!");
                    return None;
                }

                let user = self
                    .user_repo
                    .get_user(claims.custom.id);

                match user {
                    Ok(user) => Some(UserInfo::from(user)),
                    Err(_) => None,
                }
            }
            _ => None,
        }
    }

    pub fn get_token_from_cookie_raw(&self) -> Option<String> {
        let token_cookie = self.cookies.iter().find(|c| c.name() == "token");
        token_cookie.map(|cookie| cookie.value().to_string())
    }

    pub fn get_token_from_cookie(&self) -> AuthState {
        let token_cookie = self.cookies.iter().find(|c| c.name() == "token");
        match token_cookie {
            Some(cookie) => AuthState::LoggedIn(cookie.value().to_string()),
            None => AuthState::LoggedOut,
        }
    }

    pub fn render_protected_route(&mut self, template: impl Template) -> ActixResult<HttpResponse> {
        if !self.user_logged_in() {
            let denied_template = AccessDeniedTemplate {};
            let body = denied_template.render().map_err(ErrorInternalServerError)?;

            let redirect_url = "/access_denied".to_string();

            return Ok(HttpResponse::SeeOther()
                .content_type("text/html")
                .append_header(("Location", redirect_url))
                .body(body));
        }

        let body = template.render().map_err(ErrorInternalServerError)?;
        Ok(HttpResponse::Ok().content_type("text/html").body(body))
    }
}

fn as_ticks(val: Option<Duration>) -> u64 {
    // This call will never fail!
    match val {
        Some(x) => x.as_ticks(),
        None => unreachable!(), // ! Coerced to u64.
    }
}

#[derive(PartialEq)]
pub enum AuthState {
    LoggedIn(String),
    LoggedOut,
}

#[utoipa::path(
responses(
(status = 202, description = "Token was accepted and recognized"),
(status = 400, description = "Input params incorrect"),
(status = 403, description = "Token not accepted"),
(status = 501, description = "unknown error"),
)
)]
#[post("/try_token")]
pub async fn try_token(
    data: HttpRequest,
    key: Data<HS256Key>,
    user_repo: Data<PgUserRepository>,
) -> ActixResult<HttpResponse> {
    let cookies = data.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let user_info = auth.get_user_info_from_token();

    if user_info.is_none() {
        return Ok(HttpResponse::Forbidden()
            .content_type("text/html")
            .body("token not recognized"));
    }
    Ok(HttpResponse::Accepted()
        .content_type("json")
        .json(user_info.unwrap()))
}

pub fn get_login_error_template_response(error_message: String) -> ActixResult<HttpResponse> {
    let login_error_template = LoginPageContentTemplate {
        already_logged_in: false,
        login_error_message: Some(error_message),
    };

    let login_error_body = login_error_template
        .render()
        .map_err(ErrorInternalServerError)?;

    return Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body(login_error_body));
}
