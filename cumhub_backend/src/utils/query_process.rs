use crate::models::VideoWithOtherData;
use crate::repositories::video::models::SelectVideoManyFilter;
use crate::repositories::video::repository::{PgVideoRepository, VideoRepository};
use actix_web::web::{Data};

pub fn filter_videos(
    parsed_params: SelectVideoManyFilter,
    video_repo: Data<PgVideoRepository>,
    include_premium: bool,
) -> Vec<VideoWithOtherData> {
    let mut video_filter = SelectVideoManyFilter::new();
    video_filter.title_substring = parsed_params.title_substring.clone();
    video_filter.category = parsed_params.category.clone();
    video_filter.include_premium = Some(include_premium);
    video_filter.sort_by = parsed_params.sort_by.clone();

    video_repo.get_videos(video_filter).unwrap_or_default()
}
