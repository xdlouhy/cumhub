use actix_web::http::StatusCode;
use actix_web::{dev, HttpResponse, ResponseError};

use awc::error::{
  ConnectError, SendRequestError as AwcSendRequestError,
};
use awc::ClientResponse;

use std::fmt;

#[derive(Debug)]
pub struct SendRequestError(AwcSendRequestError);

impl fmt::Display for SendRequestError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "{}", self.0)
  }
}

impl From<AwcSendRequestError> for SendRequestError {
  fn from(e: AwcSendRequestError) -> Self {
    Self(e)
  }
}

impl ResponseError for SendRequestError {
  fn status_code(&self) -> StatusCode {
    match self.0 {
      AwcSendRequestError::Connect(ConnectError::Timeout) => {
        StatusCode::GATEWAY_TIMEOUT
      }
      AwcSendRequestError::Connect(_) => StatusCode::BAD_REQUEST,
      _ => StatusCode::INTERNAL_SERVER_ERROR,
    }
  }
}

pub trait IntoHttpResponse {
  fn into_http_response(self) -> HttpResponse;

  fn into_wrapped_http_response<E>(self) -> Result<HttpResponse, E>
  where
    Self: Sized,
  {
    Ok(self.into_http_response())
  }
}

impl IntoHttpResponse
  for ClientResponse<dev::Decompress<dev::Payload>>
{
  fn into_http_response(self) -> HttpResponse {
    let mut response = HttpResponse::build(self.status());

    self.headers().into_iter().for_each(|(k, v)| {
      response.insert_header((k, v));
    });

    response.streaming(self)
  }
}