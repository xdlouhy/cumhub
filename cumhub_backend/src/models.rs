use std::io::Write;

use chrono::{DateTime, Utc};
use diesel::deserialize::FromSql;
use diesel::pg::{Pg, PgValue};
use diesel::serialize::{IsNull, Output, ToSql};
use diesel::{AsExpression, FromSqlRow, Insertable, Queryable, QueryableByName, Selectable};
use serde::{Deserialize, Serialize};
use strum_macros::{Display, EnumIter, EnumString};
use utoipa::ToSchema;
use uuid::Uuid;

#[derive(Serialize, Deserialize, Queryable, Selectable, ToSchema)]
#[diesel(table_name = crate::schema::channel_info)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct ChannelInfo {
    pub id: Uuid,
    pub username: String,
    pub description: Option<String>,
    pub profile_picture: Option<String>,
    pub subscribers: i64,
}

#[derive(Serialize, Deserialize, Queryable, Selectable, ToSchema)]
#[diesel(table_name = crate::schema::user)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct User {
    pub id: Uuid,
    pub username: Option<String>,
    pub password_hash: String,
    pub email: Option<String>,
    pub description: Option<String>,
    pub profile_pic_url: Option<String>,
    pub premium_expiration: Option<DateTime<Utc>>,
    pub created_at: DateTime<Utc>,
    pub deleted_at: Option<DateTime<Utc>>,
}

#[derive(
    Serialize,
    Deserialize,
    Debug,
    AsExpression,
    FromSqlRow,
    Clone,
    PartialEq,
    ToSchema,
    EnumString,
    Display,
    EnumIter,
)]
#[diesel(sql_type = crate::schema::sql_types::Category)]
#[serde(rename_all = "snake_case")]
pub enum Category {
    Vanilla,
    Bdsm,
    Lesbian,
    Gay,
    Fetish,
    Femdom,
    Anal,
    Oral,
    Group,
    Solo,
}
impl Category {
    pub fn get_thumbnail(&self) -> &str {
        match *self {
            Category::Vanilla => "https://qph.cf2.quoracdn.net/main-qimg-9093d84ad23d5f40f54835fadc14cd7c-lq",
            Category::Bdsm => "https://upload.wikimedia.org/wikipedia/commons/4/48/Model_in_bondage.jpg",
            Category::Lesbian => "https://img.huffingtonpost.com/asset/57eed6bf170000f70aac8959.jpeg?ops=scalefit_960_noupscale",
            Category::Anal => "https://www.prioritystdtesting.com/wp-content/uploads/2019/06/stds-from-anal-sex-768x424.jpeg",
            Category::Oral => "https://previews.123rf.com/images/lculig/lculig0911/lculig091100006/5880229-photo-of-a-girl-having-a-frankfurter-hot-dog-in-her-mouth.jpg",
            Category::Femdom => "https://m.media-amazon.com/images/I/71WEX0b5AwL._AC_UY1000_.jpg",
            Category::Fetish => "https://m.media-amazon.com/images/I/71WEX0b5AwL._AC_UY1000_.jpg",
            Category::Group => "https://external-preview.redd.it/M7p7d8mkMIUksfk_B4qSzDv39E1-nDlGEJ20FOQX8ck.png?auto=webp&s=f18f5df821ee0304a3d7738dc0bfbf80bc571ae5",
            Category::Solo => "https://i.kym-cdn.com/photos/images/newsfeed/000/020/356/sfw-porn-1225101702-97208.jpg?1253736537",
            _ => "https://qph.cf2.quoracdn.net/main-qimg-9093d84ad23d5f40f54835fadc14cd7c-lq"
        }
    }
}
impl ToSql<crate::schema::sql_types::Category, Pg> for Category {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Pg>) -> diesel::serialize::Result {
        match *self {
            Category::Vanilla => out.write_all(b"VANILLA")?,
            Category::Bdsm => out.write_all(b"BDSM")?,
            Category::Lesbian => out.write_all(b"LESBIAN")?,
            Category::Gay => out.write_all(b"GAY")?,
            Category::Fetish => out.write_all(b"FETISH")?,
            Category::Femdom => out.write_all(b"FEMDOM")?,
            Category::Anal => out.write_all(b"ANAL")?,
            Category::Oral => out.write_all(b"ORAL")?,
            Category::Group => out.write_all(b"GROUP")?,
            Category::Solo => out.write_all(b"SOLO")?,
        }

        Ok(IsNull::No)
    }
}


impl FromSql<crate::schema::sql_types::Category, Pg> for Category {
    fn from_sql(bytes: PgValue) -> diesel::deserialize::Result<Self> {
        match bytes.as_bytes() {
            b"VANILLA" => Ok(Category::Vanilla),
            b"BDSM" => Ok(Category::Bdsm),
            b"LESBIAN" => Ok(Category::Lesbian),
            b"GAY" => Ok(Category::Gay),
            b"FETISH" => Ok(Category::Fetish),
            b"FEMDOM" => Ok(Category::Femdom),
            b"ANAL" => Ok(Category::Anal),
            b"ORAL" => Ok(Category::Oral),
            b"GROUP" => Ok(Category::Group),
            b"SOLO" => Ok(Category::Solo),
            _ => Err("unrecognized value".into()),
        }
    }
}

#[derive(Serialize, Deserialize, Queryable, Selectable, ToSchema)]
#[diesel(table_name = crate::schema::video)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Video {
    pub id: Uuid,
    pub author_id: Uuid,
    pub title: String,
    pub description: Option<String>,
    pub url: String,
    pub views: i32,
    /// In seconds
    pub length: i32,
    pub is_premium: bool,
    pub uploaded_at: DateTime<Utc>,
    pub deleted_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Deserialize, Selectable, Queryable, QueryableByName, ToSchema)]
#[diesel(table_name = crate::schema::video_details_view)]
pub struct VideoWithOtherData {
    pub id: Uuid,
    pub author_id: Uuid,
    pub author_username: String,
    pub title: String,
    pub description: Option<String>,
    pub url: String,
    pub views: i32,
    /// In seconds
    pub length: i32,
    pub is_premium: bool,
    pub uploaded_at: DateTime<Utc>,
    pub deleted_at: Option<DateTime<Utc>>,
    pub categories: Vec<Category>,
    pub likes: i64,
}

#[derive(Serialize, Deserialize, Queryable, Selectable, Insertable, ToSchema)]
#[diesel(table_name = crate::schema::video_categories)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct VideoCategory {
    pub video_id: Uuid,
    pub category: Category,
}

#[derive(Serialize, Deserialize, Queryable, Selectable, ToSchema)]
#[diesel(table_name = crate::schema::rating)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Rating {
    pub author_id: Uuid,
    pub video_id: Uuid,
    pub liked: bool,
    pub created_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Queryable, Selectable, ToSchema, Clone)]
#[diesel(table_name = crate::schema::comment)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Comment {
    pub id: Uuid,
    pub author_id: Uuid,
    pub video_id: Uuid,
    pub content: String,
    pub created_at: DateTime<Utc>,
    pub deleted_at: Option<DateTime<Utc>>,
}

#[derive(Serialize, Deserialize, Queryable, Selectable, ToSchema, Clone)]
#[diesel(table_name = crate::schema::comment_details_view)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct CommentWithOtherData {
    pub id: Uuid,
    pub author_id: Uuid,
    pub author_username: String,
    pub video_id: Uuid,
    pub content: String,
    pub created_at: DateTime<Utc>,
    pub deleted_at: Option<DateTime<Utc>>,
}

#[derive(Serialize, Deserialize, Queryable, Selectable, ToSchema, Clone)]
#[diesel(table_name = crate::schema::subscription)]
pub struct Subscription {
    pub subscriber_id: Uuid,
    pub author_id: Uuid,
    pub created_at: DateTime<Utc>,
}
