use actix_web::{delete, get, HttpResponse, patch, post, Responder, web};
use actix_web::web::{Data, Query};
use uuid::Uuid;
use web::{Json, Path};

use crate::errors::ApiError;
use crate::repositories::comment::models::{NewComment, PartialComment, SelectCommentManyFilter};
use crate::repositories::comment::repository::{CommentRepository, PgCommentRepository};

#[utoipa::path(
request_body = NewComment,
responses(
(status = 201, description = "Saves new comment to database!"),
(status = 400, description = "Specified comment cannot be created!")
)
)]
#[post("/api/comment")]
pub async fn create_comment(
    comment_repo: Data<PgCommentRepository>,
    create_comment: Json<NewComment>,
) -> impl Responder {
    let new_comment = comment_repo.create_comment(create_comment.0);
    match new_comment {
        Ok(comment) => HttpResponse::Created().json(comment),
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
    responses(
        (status = 200, description = "Retrieves comment by id!"),
        (status = 400, description = "Specified comment cannot be fetched!")
    )
)]
#[get("/api/comment/{id}")]
pub async fn get_comment(
    comment_repo: Data<PgCommentRepository>,
    comment_id: Path<Uuid>
) -> impl Responder {
    let found_comment = comment_repo.get_comment(comment_id.into_inner());
    match found_comment {
        Ok(comment) => HttpResponse::Ok().json(comment),
        Err(err) => ApiError::from(err).into(),
    }
}


#[utoipa::path(
params(SelectCommentManyFilter),
responses(
(status = 200, description = "Retrieves filtered comments!"),
(status = 204, description = "No Comments matched filters"),
(status = 400, description = "Specified comment cannot be fetched!")
)
)]
#[get("/api/comments")]
pub async fn get_comments(
    comment_repo: Data<PgCommentRepository>,
    filter: Query<SelectCommentManyFilter>,
) -> impl Responder {
    let obtained_comments = comment_repo.get_comments(filter.0);

    match obtained_comments {
        Ok(comments) => {
            if comments.is_empty() {
                return HttpResponse::NoContent().body(());
            }
            HttpResponse::Ok().json(comments)
        }
        Err(err) => ApiError::from(err).into(),
    }
}


#[utoipa::path(
responses(
(status = 200, description = "Comment with edited values"),
(status = 400, description = "Specified comment cannot be edited!")
)
)]
#[patch("/api/comment/{id}")]
pub async fn edit_comment(
    comment_repo: Data<PgCommentRepository>,
    comment_id: Path<Uuid>,
    comment_edit: Json<PartialComment>,
) -> impl Responder {
    let edited_comment = comment_repo.edit_comment(comment_id.into_inner(), comment_edit.0);
    match edited_comment {
        Ok(comment) => HttpResponse::Ok().json(comment),
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
responses(
(status = 204, description = "Comment was deleted"),
(status = 400, description = "Specified comment cannot be deleted!")
)
)]
#[delete("/api/comment/{id}")]
pub async fn delete_comment(
    comment_repo: Data<PgCommentRepository>,
    comment_id: Path<Uuid>,
) -> impl Responder {
    let deleted_comment = comment_repo.delete_comment(comment_id.into_inner());
    match deleted_comment {
        Ok(_comment) => HttpResponse::NoContent().body(()),
        Err(err) => ApiError::from(err).into(),
    }
}