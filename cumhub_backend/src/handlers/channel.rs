use actix_web::{get, HttpResponse, Responder};
use actix_web::web::{Data, Path, Query};
use uuid::Uuid;
use crate::errors::ApiError;
use crate::repositories::channel::models::SelectChannelsManyFilter;
use crate::repositories::channel::repository::{ChannelsRepository, PgChannelRepository};

#[utoipa::path(
responses(
(status = 200, description = "Retrieves channel by id!"),
(status = 400, description = "Specified channel cannot be fetched!")
)
)]
#[get("/api/channel/{id}")]
pub async fn get_channel(
    channel_repo: Data<PgChannelRepository>,
    channel_id: Path<Uuid>
) -> impl Responder {
    let found_channel = channel_repo.get_channel(channel_id.into_inner());
    match found_channel {
        Ok(channel) => HttpResponse::Ok().json(channel),
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
params(SelectChannelsManyFilter),
responses(
(status = 200, description = "Retrieves filtered channels!"),
(status = 204, description = "No Channels matched filters"),
(status = 400, description = "Specified channel cannot be fetched!")
)
)]
#[get("/api/channels")]
pub async fn get_channels(
    channel_repo: Data<PgChannelRepository>,
    filter: Query<SelectChannelsManyFilter>,
) -> impl Responder {
    let obtained_channels = channel_repo.get_channels(filter.0);

    match obtained_channels {
        Ok(channels) => {
            if channels.is_empty() {
                return HttpResponse::NoContent().body(());
            }
            HttpResponse::Ok().json(channels)
        }
        Err(err) => ApiError::from(err).into(),
    }
}