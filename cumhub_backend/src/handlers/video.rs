use std::io::Read;
use std::str::FromStr;
use std::env;
use std::ops::Deref;

use crate::errors::ApiError;
use crate::repositories::video::models::{NewVideo, SelectVideoManyFilter};
use crate::repositories::video::repository::{PgVideoRepository, VideoRepository};
use crate::utils::actix_proxy::IntoHttpResponse;
use actix_multipart::form::text::Text;
use actix_web::http::header::ContentType;
use actix_web::web::{Data, Bytes, Path, Query};
use actix_multipart::form::tempfile::TempFile;
use actix_multipart::form::MultipartForm;
use actix_web::{get, post, web, HttpResponse, Responder, delete, HttpRequest};
use actix_web::error::ErrorInternalServerError;
use askama::Template;
use uuid::Uuid;
use futures_util::stream::once;
use jwt_simple::algorithms::HS256Key;
use log::info;
use utoipa::ToSchema;
use crate::models::Category;
use crate::repositories::user::repository::PgUserRepository;
use crate::templates::UploadSuccessfulTemplate;
use crate::utils::auth::AuthHandler;


#[derive(MultipartForm, ToSchema)]
pub struct VideoFormData {
  title: Text<String>,
  description: Text<String>,
  category: Text<String>,
  is_premium: Text<String>,
  video: TempFile
}

#[utoipa::path(
    request_body = VideoFormData,
    responses(
    (status = 201, description = "Saves new video record to database!"),
    (status = 400, description = "Specified video cannot be created!")
    )
)]
#[post("/video")]
pub async fn post_video(
    client: Data<awc::Client>,
    repo: Data<PgVideoRepository>,
    user_repo: Data<PgUserRepository>,
    key: Data<HS256Key>,
    content: MultipartForm<VideoFormData>,
    data: HttpRequest,
    
) -> impl Responder {

    let api_key = env::var("FILESERVER_API_KEY")
    .unwrap_or("69".to_string());
    let title = content.title.clone(); 
    let description = content.description.clone();
    info!("pre premium ");
    let is_premium = content.is_premium.clone();
    info!("post premium ");
    let category_str = content.category.clone();
    let category = Category::from_str(&category_str).unwrap();
    let video = content.video.file.reopen();
    let video_bytes =  match video {
      Ok(file) => {
        file.bytes().map(|b| b.unwrap_or_default())
      },
      Err(_err) => return HttpResponse::InternalServerError().finish()
    };

    let cookies = data.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let user_info = auth.get_user_info_from_token();
    let user_id = match user_info {
      Some(user) => user.id,
      None => return HttpResponse::Unauthorized().finish()
    };

    let categories = vec![category.clone()];

    let fileserver_base_url = env::var("FILESERVER_URL").unwrap_or("fileserver url not set in .env!".to_string());
    let mut metadata = NewVideo {
      title: title.clone(),
      description,
      categories,
      is_premium: is_premium == "on",
      length: 0,
      author_id: user_id,
      url: category.get_thumbnail().to_string(),
    };
   
    let db_response = repo.create_video(metadata.clone());
    let video_entry = match db_response {
      Ok(entry) => entry,
      Err(_err) => {
        return HttpResponse::InternalServerError().finish()}
    };
  
    let id = video_entry.id;
    let url = format!("{fileserver_base_url}/{id}.mp4");

    let response = client
      .put(&url)
      .append_header(("AccessKey", api_key))
      .append_header(ContentType::octet_stream())
      .append_header(("accept", "application/json"))
      .timeout(std::time::Duration::from_secs(1800))
      .send_stream(once(async { Ok::<_, std::io::Error>(Bytes::from_iter(video_bytes)) }))
      .await;
  
    metadata.url = url.clone();
    match response {
      Ok(_response) =>     {
        let template = UploadSuccessfulTemplate {
              recorded_video: title.clone(),
          };
        let body = template.render().map_err(ErrorInternalServerError).unwrap();
        let redirect_url = "/upload?state=success".to_string();
        HttpResponse::Ok()
           .content_type("text/html")
           .append_header(("HX-Push-Url", redirect_url))
           .body(body)}
      Err(_err) => { 
         HttpResponse::NotFound().finish() 
        }
    }
}
  #[utoipa::path(
    responses(
    (status = 200, description = "Retrieves filtered video records!"),
    (status = 204, description = "No videos matched filters"),
    (status = 400, description = "Specified videos cannot be fetched!")
    )
    )]
#[get("/api/video/{id}")]
pub async fn get_video(
    client: Data<awc::Client>,
    path: web::Path<(String,)>,
) -> impl Responder {
  let id = path.into_inner().0;
  let fileserver_base_url = env::var("FILESERVER_URL")
  .unwrap_or("fileserver url not set in .env!".to_string());
  let api_key = env::var("FILESERVER_API_KEY").unwrap_or("69".to_string());

  let url = format!("{fileserver_base_url}/{id}.mp4");

  let response = client.get(&url)
  .append_header(("AccessKey", api_key))
  .send()
  .await;

  match response {
    Ok(response) => response.into_http_response(),
    Err(_err) => HttpResponse::NotFound().finish()
  }
}

#[utoipa::path(
params(SelectVideoManyFilter),
responses(
(status = 200, description = "Retrieves filtered video records!"),
(status = 204, description = "No videos matched filters"),
(status = 400, description = "Specified videos cannot be fetched!")
)
)]
#[get("/api/videos")]
pub async fn get_videos(
    video_repo: Data<PgVideoRepository>,
    filter: Query<SelectVideoManyFilter>,
) -> impl Responder {
    let obtained_videos = video_repo.get_videos(filter.0);

    match obtained_videos {
        Ok(video) => {
            if video.is_empty() {
                return HttpResponse::NoContent().body(());
            }
            HttpResponse::Ok().json(video)
        }
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
    responses(
    (status = 204, description = "Video record was deleted"),
    (status = 400, description = "Specified comment cannot be deleted!")
    )
    )]
    #[delete("/api/video/{id}")]
    pub async fn delete_video(
        video_repo: Data<PgVideoRepository>,
        video_id: Path<Uuid>,
    ) -> impl Responder {
    
        let deleted_video = video_repo.delete_video(video_id.into_inner());
        match deleted_video {
            Ok(_video) => HttpResponse::NoContent().json(()),
            Err(err) => ApiError::from(err).into()
        }
}

