use actix_web::{delete, get, HttpResponse, patch, post, Responder, web};
use actix_web::web::Data;
use log::info;
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};
use uuid::Uuid;
use web::{Json, Path};


use crate::errors::ApiError;
use crate::repositories::user::models::{NewUser, PartialUser};
use crate::repositories::user::repository::{PgUserRepository, UserRepository};
use crate::utils::auth::hash_password;

#[utoipa::path(
request_body = NewUserRequest,
responses(
(status = 201, description = "Saves new user to database!"),
(status = 400, description = "Specified user cannot be created!")
)
)]
#[post("/api/user")]
pub async fn create_user(
    user_repo: Data<PgUserRepository>,
    create_user: Json<NewUserRequest>,
) -> impl Responder {
    let new_user = user_repo.create_user(create_user.0.into());
    match new_user {
        Ok(user) => HttpResponse::Created().json(user),
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
responses(
(status = 200, description = "Retrieves user by id!"),
(status = 400, description = "Specified user cannot be fetched!")
)
)]
#[get("/api/user/{id}")]
pub async fn get_user(
    user_repo: Data<PgUserRepository>,
    user_id: Path<Uuid>,
) -> impl Responder {
    let found_user = user_repo.get_user(user_id.into_inner());
    match found_user {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
responses(
(status = 200, description = "Retrieves user by username!"),
(status = 400, description = "Specified user cannot be fetched!")
)
)]
#[get("/api/user/username/{username}")]
pub async fn get_user_by_username(
    user_repo: Data<PgUserRepository>,
    username_value: Path<String>,
) -> impl Responder {

    let found_user = user_repo.get_user_by_username(username_value.into_inner());
    match found_user {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
responses(
(status = 200, description = "User with edited values"),
(status = 400, description = "Specified user cannot be edited!")
)
)]
#[patch("/api/user/{id}")]
pub async fn edit_user(
    user_repo: Data<PgUserRepository>,
    user_id: Path<Uuid>,
    user_edit: Json<PartialUser>,
) -> impl Responder {

    let edited_user = user_repo.edit_user(user_id.into_inner(), user_edit.0);
    match edited_user {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
    responses(
    (status = 200, description = "User with edited values"),
    (status = 400, description = "Specified user cannot be edited!")
    )
    )]
    #[post("/api/user/{id}/premium")]
    pub async fn buy_premium(
        user_repo: Data<PgUserRepository>,
        user_id: Path<Uuid>,
    ) -> impl Responder {
        let id = user_id.into_inner();
        info!("User id: {}", id);
        let user = user_repo.get_user(id);
        match user {
            Ok(mut user) => {
                user.premium_expiration = Some(user.premium_expiration.unwrap_or
                    (chrono::Utc::now()) + chrono::Duration::days(30));
                let edited_user = user_repo.edit_user(id, PartialUser::from(user));
                match edited_user {
                    Ok(user) => HttpResponse::Ok().json(user),
                    Err(err) => ApiError::from(err).into(),
                }
            },
            Err(err) => ApiError::from(err).into(),
        }
    }



#[utoipa::path(
responses(
(status = 204, description = "User was deleted"),
(status = 400, description = "Specified user cannot be deleted!")
)
)]
#[delete("/api/user/{id}")]
pub async fn delete_user(
    user_repo: Data<PgUserRepository>,
    user_id: Path<Uuid>,
) -> impl Responder {
    let deleted_user = user_repo.delete_user(user_id.into_inner());
    match deleted_user {
        Ok(_user) => HttpResponse::NoContent().body(()),
        Err(err) => ApiError::from(err).into(),
    }
}

#[derive(Serialize, Deserialize, ToSchema, IntoParams)]
pub struct NewUserRequest{
    pub username: String,
    pub email: String,
    pub password: String,
    pub profile_pic_url: String,
    pub description: Option<String>,
}

impl From<NewUserRequest> for NewUser {
    fn from(request: NewUserRequest) -> Self {
        // Perform the conversion logic here
        NewUser {
            username: request.username,
            email: request.email,
            profile_pic_url: request.profile_pic_url,
            password_hash: hash_password(request.password).unwrap().unprotected_as_encoded().to_string(),
            description: request.description.unwrap_or(String::from("")),
        }
    }
}