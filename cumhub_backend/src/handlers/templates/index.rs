use actix_web::web::Data;
use actix_web::{
    error::ErrorInternalServerError,
    get, post,
    web::{self},
    HttpRequest, HttpResponse, Result as ActixResult,
};
use askama::Template;
use jwt_simple::algorithms::HS256Key;
use std::ops::Deref;
use std::str::FromStr;
use strum::IntoEnumIterator;

use crate::models::Category;
use crate::repositories::user::repository::PgUserRepository;

use crate::repositories::video::models::{SelectVideoManyFilterRaw, SortBy};
use crate::repositories::video::repository::PgVideoRepository;
use crate::templates::IndexTemplate;

use crate::utils::auth::AuthHandler;
use crate::utils::params_mapper::{
    map_select_video_many_filter_raw_to_mapper, map_sort_by_to_string,
};

use crate::utils::query_process::filter_videos;

#[get("/")]
pub async fn get_index(
    data: HttpRequest,
    video_repo: Data<PgVideoRepository>,
    key: Data<HS256Key>,
    user_repo: Data<PgUserRepository>,
) -> ActixResult<HttpResponse> {
    let parsed_params =
        web::Query::<SelectVideoManyFilterRaw>::from_query(data.query_string()).unwrap();

    let cookies = data.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let user_info = auth.get_user_info_from_token();

    let mapped_params = map_select_video_many_filter_raw_to_mapper(parsed_params.clone());

    let videos = filter_videos(mapped_params.clone(), video_repo, auth.is_user_premium());

    let template = IndexTemplate {
        categories: Category::iter().map(|c| c.to_string()).collect(),
        category: mapped_params
            .category
            .clone()
            .unwrap_or(Category::Vanilla)
            .to_string(),
        title_substring: mapped_params
            .title_substring
            .clone()
            .unwrap_or(String::from("")),
        sort_by: map_sort_by_to_string(mapped_params.sort_by.clone().unwrap_or(SortBy::Newest)),
        already_logged_in: auth.user_logged_in(),
        user_info,
        videos,
    };

    let body = template.render().map_err(ErrorInternalServerError)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

#[derive(serde::Deserialize)]
pub struct FilterVideosForm {
    search: String,
    sort: String,
}

#[post("/")]
pub async fn filter_index_videos(
    user_input: web::Form<FilterVideosForm>,
    data: HttpRequest,
    video_repo: Data<PgVideoRepository>,
    user_repo: Data<PgUserRepository>,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let parsed_params =
        web::Query::<SelectVideoManyFilterRaw>::from_query(data.query_string()).unwrap();

    let mut mapped_params = map_select_video_many_filter_raw_to_mapper(parsed_params.clone());
    mapped_params.title_substring = Some(user_input.search.clone());
    mapped_params.sort_by =
        Some(SortBy::from_str(user_input.sort.clone().as_str()).unwrap_or(SortBy::Newest));

    let cookies = data.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);

    let videos = filter_videos(mapped_params.clone(), video_repo, auth.is_user_premium());

    let template = crate::templates::VideosTemplate { videos };

    let body = template.render().map_err(ErrorInternalServerError)?;

    let redirect_url = format!(
        "/?category={}&title_substring={}&sort_by={}",
        mapped_params.category.clone().unwrap_or(Category::Vanilla),
        user_input.search,
        user_input.sort
    );

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .append_header(("HX-Push-Url", redirect_url))
        .body(body))
}
