use crate::{
    repositories::user::repository::PgUserRepository,
    templates::{UploadSuccessfulTemplate, UploadTemplate},
    utils::auth::AuthHandler,
};
use actix_easy_multipart::MultipartForm;
use actix_easy_multipart::{tempfile::Tempfile, text::Text};
use actix_web::web::Data;
use actix_web::{
    error::ErrorInternalServerError, get, post, web, HttpRequest, HttpResponse,
    Result as ActixResult,
};
use askama::Template;
use jwt_simple::algorithms::HS256Key;
use serde::{Deserialize, Serialize};
use std::ops::Deref;

#[derive(MultipartForm)]
pub struct UploadVideoForm {
    name: Text<String>,
    description: Text<String>,
    video: Tempfile,
    category: Text<String>,
}

#[derive(Serialize, Deserialize)]
struct UploadParams {
    state: Option<String>,
}

#[get("/upload")]
pub async fn get_upload(
    request: HttpRequest,
    key: Data<HS256Key>,
    user_repo: Data<PgUserRepository>,
) -> ActixResult<HttpResponse> {
    let cookies = request.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let mut auth_handler = AuthHandler::new(cookies, key.deref(), &user_repo);

    let uploaded = match web::Query::<UploadParams>::from_query(request.query_string()) {
        Ok(params) => params.state == Some(String::from("success")),
        Err(_) => false,
    };

    let template = UploadTemplate { uploaded };

    auth_handler.render_protected_route(template)
}

#[post("/upload")]
pub async fn upload_video(
    data: MultipartForm<UploadVideoForm>,
    _: HttpRequest,
) -> ActixResult<HttpResponse> {
    let video = &data.video;


    // // TODO add check if content type is video
    // let content_type = match video.content_type.clone() {

    //     },
    //     None => return Ok(HttpResponse::BadRequest().body("No content type provided")),
    // };

    let is_video = true;

    if video.size == 0 || !is_video {
        return Ok(HttpResponse::BadRequest().body("No video provided"));
    }

    let template = UploadSuccessfulTemplate {
        recorded_video: video.file.path().to_str().to_owned().unwrap().to_string(),
    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    let redirect_url = "/upload?state=success".to_string();

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .append_header(("HX-Push-Url", redirect_url))
        .body(body))
}
