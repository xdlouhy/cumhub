use crate::{
    repositories::user::{
        models::PartialUser,
        repository::{PgUserRepository, UserRepository},
    },
    templates::{PremiumSubscribedSuccessfullyTemplate, PremiumTemplate},
    utils::auth::{redirect_access_denied, AuthHandler},
};
use actix_web::{
    error::ErrorInternalServerError, get, post, web::Data, HttpRequest, HttpResponse,
    Result as ActixResult,
};
use askama::Template;
use jwt_simple::algorithms::HS256Key;
use std::{ops::Deref};

#[get("/premium")]
pub async fn get_premium(
    data: HttpRequest,
    key: Data<HS256Key>,
    user_repo: Data<PgUserRepository>,
) -> ActixResult<HttpResponse> {
    let cookies = data.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let mut auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let user_info = auth.get_user_info_from_token();

    let template = PremiumTemplate { user_info };

    auth.render_protected_route(template)
}

#[post("/premium/subscribe")]
pub async fn post_premium_subscribe(
    data: HttpRequest,
    user_repo: Data<PgUserRepository>,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let auth = AuthHandler::new(
        data.cookies().map(|c| c.to_owned()).unwrap_or_default(),
        &key,
        &user_repo,
    );

    let id = match auth.get_user_info_from_token() {
        Some(user_info) => user_info.id,
        None => return redirect_access_denied(),
    };

    let user = user_repo.get_user(id);
    match user {
        Ok(mut user) => {
            user.premium_expiration = Some(
                user.premium_expiration.unwrap_or(chrono::Utc::now()) + chrono::Duration::days(30),
            );
            let edited_user = user_repo.edit_user(id, PartialUser::from(user));
            match edited_user {
                Ok(_) => {
                    let template = PremiumSubscribedSuccessfullyTemplate {};
                    let body = template.render().map_err(ErrorInternalServerError)?;
                    Ok(HttpResponse::Ok().content_type("text/html").body(body))
                }
                Err(_) => Ok(HttpResponse::InternalServerError()
                    .content_type("text/html")
                    .body("Error editing user")),
            }
        }
        Err(_) => Ok(HttpResponse::InternalServerError()
            .content_type("text/html")
            .body("Could not find user")),
    }
}
