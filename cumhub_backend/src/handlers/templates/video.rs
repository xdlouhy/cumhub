use std::ops::Deref;

use actix_web::{
    error::ErrorInternalServerError,
    get, post,
    web::{Data, Form, Path},
    HttpRequest, HttpResponse, Result as ActixResult,
};
use askama::Template;
use jwt_simple::algorithms::HS256Key;
use strum::IntoEnumIterator;
use uuid::Uuid;

use crate::{handlers::templates::errors::bad_request_page, templates::VideoStatsTemplate};
use crate::{
    models::Category,
    repositories::{
        channel::repository::{ChannelsRepository, PgChannelRepository},
        comment::{
            models::{NewComment, SelectCommentManyFilter},
            repository::{CommentRepository, PgCommentRepository},
        },
        rating::{
            models::{NewRating, RatingId},
            repository::{PgRatingRepository, RatingRepository},
        },
        user::repository::PgUserRepository,
        video::repository::{PgVideoRepository, VideoRepository},
    },
    templates::{CommentsTemplate, VideoPageTemplate},
    utils::auth::AuthHandler,
};

#[get("/video/{id}")]
pub async fn get_video(
    data: HttpRequest,
    path: Path<String>,
    video_repo: Data<PgVideoRepository>,
    comment_repo: Data<PgCommentRepository>,
    user_repo: Data<PgUserRepository>,
    channel_repo: Data<PgChannelRepository>,
    rating_repo: Data<PgRatingRepository>,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let id_str = path.into_inner();
    let video_id = match Uuid::parse_str(&id_str) {
        Ok(u) => u,
        Err(e) => {
            return bad_request_page(format!("Error extracting path parameters: {}", e)).await;
        }
    };
    let cookies = data.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let user_info = auth.get_user_info_from_token();

    let video = video_repo
        .get_video(video_id)
        .map_err(ErrorInternalServerError)?;

    let channel_info = channel_repo
        .get_channel(video.author_id)
        .map_err(ErrorInternalServerError)?;

    let comments = comment_repo
        .get_comments(SelectCommentManyFilter {
            video_id: Some(video_id),
            author_id: None,
            pagination: None,
        })
        .map_err(ErrorInternalServerError)?;

    let user_rating = rating_repo
        .get_rating(video_id, video.author_id)
        .map_err(ErrorInternalServerError)?;

    let already_liked = match &user_rating {
        Some(rating) => match rating.liked {
            true => Some(true),
            _ => Some(false),
        },
        None => None,
    };

    let already_disliked = match &user_rating {
        Some(rating) => match rating.liked {
            false => Some(true),
            _ => Some(false),
        },
        None => None,
    };

    let template = VideoPageTemplate {
        video,
        already_disliked,
        already_liked,
        channel_info,
        comments,
        user_info,
        user_rating,
        categories: Category::iter().map(|c| c.to_string()).collect(),
        already_logged_in: auth.user_logged_in(),
    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    video_repo
        .increment_view_count(video_id)
        .map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

#[derive(serde::Deserialize)]
pub struct PostCommentForm {
    content: String,
}

#[post("/video/{id}/comment")]
pub async fn post_comment_to_video(
    request: HttpRequest,
    key: Data<HS256Key>,
    path: Path<String>,
    user_input: Form<PostCommentForm>,
    comment_repo: Data<PgCommentRepository>,
    user_repo: Data<PgUserRepository>,
) -> ActixResult<HttpResponse> {
    let id_str = path.into_inner();
    let video_id = match Uuid::parse_str(&id_str) {
        Ok(u) => u,
        Err(e) => return Err(ErrorInternalServerError(e)),
    };

    let cookies = request.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);

    let user_info = match auth.get_user_info_from_token() {
        Some(user) => user,
        None => return Ok(HttpResponse::BadRequest().body("User not found".to_string())),
    };

    let author_id = user_info.id;

    let new_comment = NewComment {
        video_id,
        author_id,
        content: user_input.content.clone(),
    };

    let _ = comment_repo.create_comment(new_comment);

    // Fetch updated list of comments
    let updated_comments = comment_repo
        .get_comments(SelectCommentManyFilter {
            video_id: Some(video_id),
            author_id: None,
            pagination: None,
        })
        .map_err(ErrorInternalServerError)?;

    // Render only the comments list part of the page
    let template = CommentsTemplate {
        comments: updated_comments,
    };
    let body = template.render().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

#[post("/video/{id}/like")]
pub async fn post_like_to_video(
    request: HttpRequest,
    key: Data<HS256Key>,
    path: Path<String>,
    rating_repo: Data<PgRatingRepository>,
    user_repo: Data<PgUserRepository>,
    video_repo: Data<PgVideoRepository>,
) -> ActixResult<HttpResponse> {
    post_rating_to_video(request, key, path, true, rating_repo, user_repo, video_repo).await
}

#[post("/video/{id}/dislike")]
pub async fn post_dislike_to_video(
    request: HttpRequest,
    key: Data<HS256Key>,
    path: Path<String>,
    rating_repo: Data<PgRatingRepository>,
    user_repo: Data<PgUserRepository>,
    video_repo: Data<PgVideoRepository>,
) -> ActixResult<HttpResponse> {
    post_rating_to_video(
        request,
        key,
        path,
        false,
        rating_repo,
        user_repo,
        video_repo,
    )
    .await
}

pub async fn post_rating_to_video(
    request: HttpRequest,
    key: Data<HS256Key>,
    path: Path<String>,
    rating: bool,
    rating_repo: Data<PgRatingRepository>,
    user_repo: Data<PgUserRepository>,
    video_repo: Data<PgVideoRepository>,
) -> ActixResult<HttpResponse> {
    let id_str = path.into_inner();
    let video_id = match Uuid::parse_str(&id_str) {
        Ok(u) => u,
        Err(e) => return Err(ErrorInternalServerError(e)),
    };

    let cookies = request.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);

    let user_info = match auth.get_user_info_from_token() {
        Some(user) => user,
        None => return Ok(HttpResponse::BadRequest().body("User not found".to_string())),
    };

    let author_id = user_info.id;

    let existing_rating = rating_repo
        .get_rating(video_id, author_id)
        .map_err(ErrorInternalServerError)?;

    if existing_rating.is_some() {
        // user already rated video, remove it
        let _ = rating_repo
            .delete_rating(RatingId {
                video_id,
                author_id,
            })
            .map_err(ErrorInternalServerError);
    } else {
        // user hasn't rated video yet, add it
        let _ = rating_repo
            .create_rating(NewRating {
                video_id,
                author_id,
                liked: rating,
            })
            .map_err(ErrorInternalServerError);
    }

    let existing_rating = rating_repo
        .get_rating(video_id, author_id)
        .map_err(ErrorInternalServerError)?;

    let already_liked = match &existing_rating {
        Some(rating) => match rating.liked {
            true => Some(true),
            _ => Some(false),
        },
        None => None,
    };

    let already_disliked = match &existing_rating {
        Some(rating) => match rating.liked {
            false => Some(true),
            _ => Some(false),
        },
        None => None,
    };

    let video = video_repo
        .get_video(video_id)
        .map_err(ErrorInternalServerError)?;

    let template = VideoStatsTemplate {
        video,
        already_disliked,
        already_liked,
        already_logged_in: auth.user_logged_in(),
    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}
