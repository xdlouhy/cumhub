use std::ops::Deref;

use actix_web::{error::ErrorInternalServerError, FromRequest, get, HttpRequest, HttpResponse, patch, Result as ActixResult, web};
use actix_web::web::Data;
use askama::Template;
use jwt_simple::algorithms::HS256Key;
use jwt_simple::prelude::{Deserialize, Serialize};
use utoipa::ToSchema;
use uuid::Uuid;

use crate::{templates::AccountSettingsTemplate, utils::auth::AuthHandler};
use crate::handlers::templates::errors::bad_request_page;
use crate::repositories::user::models::PartialUser;
use crate::repositories::user::repository::{PgUserRepository, UserRepository};
use crate::templates::UserInfo;
use crate::utils::auth::{hash_password, redirect_access_denied, verify_password};

#[get("/account_settings/{id}")]
pub async fn get_account_settings(
    path: web::Path<Uuid>,
    request: HttpRequest,
    key: Data<HS256Key>,
    user_repo: Data<PgUserRepository>,
) -> ActixResult<HttpResponse> {
    let cookies = request.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);

    let user_info = match auth.get_user_info_from_token() {
        Some(user) => user,
        None =>  return Ok(HttpResponse::Forbidden().body("Auth token not accepted".to_string())),
    };

    if user_info.id != path.into_inner() {
        return redirect_access_denied();
    }

    let template = AccountSettingsTemplate {
        user_id: user_info.id.to_string(),
        description: user_info.description.unwrap_or_default(),
        profile_picture: user_info.profile_picture.unwrap_or_default(),
        username: user_info.username,

        username_message: None,
        password_message: None,
        profile_pic_message: None,
        description_message: None,

        username_success: false,
        password_success: false,
        profile_pic_success: false,
        description_success: false,

    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

#[patch("/account_settings/username/{id}")]
pub async fn patch_account_username(
    user_repo: Data<PgUserRepository>,
    path: web::Path<Uuid>,
    form: web::Form<ChangeAccountForm>,
    request: HttpRequest,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let cookies = request.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let mut user_info = match auth.get_user_info_from_token() {
        Some(user) => user,
        None =>  return Ok(HttpResponse::Forbidden().body("Auth token not accepted".to_string())),
    };

    if user_info.id != *path{
        return redirect_access_denied();
    }

    if form.username.clone().is_some() {
        if let Err(err) = user_repo.edit_user(*path, form.clone().into()) {
            return redraw_template_response(user_info.clone(), Some(err.to_string()), None, None, None,
                                            false, false, false, false);
        }
    }
    user_info.username = form.username.clone().unwrap_or_default();
    redraw_template_response(user_info, None, None, None, None, true, false, false, false)
}

#[patch("/account_settings/description/{id}")]
pub async fn patch_account_description(
    user_repo: Data<PgUserRepository>,
    path: web::Path<Uuid>,
    form: web::Form<ChangeAccountForm>,
    request: HttpRequest,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let cookies = request.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let mut user_info = match auth.get_user_info_from_token() {
        Some(user) => user,
        None =>  return Ok(HttpResponse::Forbidden().body("Auth token not accepted".to_string())),
    };

    if user_info.id != *path{
        return redirect_access_denied();
    }

    if form.description.clone().is_some() {
        if let Err(err) = user_repo.edit_user(*path, form.clone().into()) {
            return redraw_template_response(user_info.clone(), None, None, None, Some(err.to_string()),
                                            false, false, false, false);
        }
    }
    user_info.description = form.description.clone();
    redraw_template_response(user_info.clone(), None, None, None, None,
                                    false, false, false, true)
}

#[patch("/account_settings/profile_pic/{id}")]
pub async fn patch_account_profile_pic(
    user_repo: Data<PgUserRepository>,
    path: web::Path<Uuid>,
    form: web::Form<ChangeAccountForm>,
    request: HttpRequest,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let cookies = request.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let mut user_info = match auth.get_user_info_from_token() {
        Some(user) => user,
        None =>  return Ok(HttpResponse::Forbidden().body("Auth token not accepted".to_string())),
    };

    if user_info.id != *path{
        return redirect_access_denied();
    }

    if form.profile_pic_url.clone().is_some() {
        if let Err(err) = user_repo.edit_user(*path, form.clone().into()) {
            return redraw_template_response(user_info.clone(), None, None, Some(err.to_string()), None,
            false, false, false, false);
        }
    }
    user_info.profile_picture = form.profile_pic_url.clone();
    redraw_template_response(user_info, None, None, None, None, false, false, true, false)
}

#[patch("/account_settings/password/{id}")]
pub async fn patch_account_password(
    user_repo: Data<PgUserRepository>,
    form: web::Form<ChangeAccountForm>,
    request: HttpRequest,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let cookies = request.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let user_info = match auth.get_user_info_from_token() {
        Some(user) => user,
        None =>  return Ok(HttpResponse::Forbidden().body("Auth token not accepted".to_string())),
    };

    let path_params_result = web::Path::<Uuid>::extract(&request).await.map_err(|err| {
        bad_request_page(format!("Error extracting path parameters: {}", err))
    });


    let id = match path_params_result {
        Ok(params) => params.into_inner(),
        Err(err) => return err.await,
    };

    if user_info.id != id{
        return redirect_access_denied();
    }

    if let Some(new_password) = form.new_password.clone() {
        if let Some(old_password) = form.old_password.clone(){
            if !verify_old_password(user_repo.clone(), id.clone(), old_password){
                return redraw_template_response(
                    user_info, None, Some("Failed to verify old password!".to_string()), None, None,
                    false, false, false, false);
            }

            let partial_user = PartialUser::edit_password_hash(hash_password(new_password).unwrap().unprotected_as_encoded().to_string(),);
            user_repo.edit_user(id.clone(), partial_user)?;
        }
    }
    redraw_template_response(user_info, None, None, None, None, false, true, false, false)
}

fn verify_old_password(user_repo: Data<PgUserRepository>, user_id: Uuid, old_password: String) -> bool {
    let user = match user_repo.get_user(user_id) {
        Ok(u) => u,
        Err(_) => return false,
    };

    let verify_result = verify_password(user.password_hash.clone(), old_password);
    verify_result.is_ok()
}

fn redraw_template_response(
    user_info: UserInfo,
    username_message: Option<String>,
    password_message: Option<String>,
    profile_pic_message: Option<String>,
    description_message: Option<String>,
    username_success: bool,
    password_success: bool,
    profile_pic_success: bool,
    description_success: bool,
) -> ActixResult<HttpResponse> {
    let template = AccountSettingsTemplate {
        user_id: user_info.id.to_string(),
        description: user_info.description.unwrap_or_default(),
        profile_picture: user_info.profile_picture.unwrap_or_default(),
        username: user_info.username,

        username_message,
        password_message,
        profile_pic_message,
        description_message,

        username_success,
        password_success,
        profile_pic_success,
        description_success,
    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

#[derive(Serialize, Deserialize, ToSchema, Clone)]
pub struct ChangeAccountForm {
    pub username: Option<String>,
    pub new_password: Option<String>,
    pub old_password: Option<String>,
    pub profile_pic_url: Option<String>,
    pub description: Option<String>,
}

impl From<ChangeAccountForm> for PartialUser {
    fn from(value: ChangeAccountForm) -> Self {
        Self {
            username: value.username,
            profile_pic_url: value.profile_pic_url,
            description: value.description,
            email: None,
            password_hash: None,
            premium_expiration: None,
        }
    }
}
