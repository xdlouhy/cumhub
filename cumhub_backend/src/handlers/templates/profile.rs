use std::ops::Deref;

use actix_web::{
    error::ErrorInternalServerError, FromRequest, get, HttpRequest, HttpResponse, post, Result as ActixResult,
    web,
};
use actix_web::web::Data;
use askama::Template;
use jwt_simple::algorithms::HS256Key;
use log::info;
use serde::{Deserialize, Serialize};
use strum::IntoEnumIterator;
use uuid::Uuid;

use crate::handlers::templates::errors::bad_request_page;
use crate::models::Category;
use crate::repositories::user::repository::{PgUserRepository, UserRepository};
use crate::repositories::video::models::{SelectVideoManyFilter, SortBy};
use crate::repositories::video::repository::{PgVideoRepository, VideoRepository};
use crate::templates::ProfileTemplate;
use crate::utils::auth::AuthHandler;

#[derive(Serialize, Deserialize)]
struct ProfileParams {
    title_substring: Option<String>,
    sort_by: Option<String>,
}

#[get("/profile/{id}")]
pub async fn get_profile(
    data: HttpRequest,
    video_repo: Data<PgVideoRepository>,
    user_repo: Data<PgUserRepository>,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let cookies = data.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);

    // if !auth.user_logged_in() {
    //     return redirect_access_denied();
    // }

    let parsed_params: web::Query<ProfileParams> =
        web::Query::from_query(data.query_string()).unwrap();

    // Extract path parameters
    let path_params_result = web::Path::<Uuid>::extract(&data).await.map_err(|err| {
        bad_request_page(format!("Error extracting path parameters: {}", err))
    });

    let id = match path_params_result {
        Ok(params) => params.into_inner(),
        Err(err) => return err.await,
    };

    let user = user_repo.get_user(id)?;
    let videos = video_repo.get_videos(SelectVideoManyFilter::by_author_id(id))?;
    info!("{}", videos.len());

    let template = ProfileTemplate {
        profile_name: user.username.unwrap_or_default(),
        profile_description: user.description.unwrap_or_default(),
        subscribers: 420690,
        videos,
        user_subscribed: true,
        already_logged_in: auth.user_logged_in(),
        channel_pic_url: user.profile_pic_url,
        user_info: auth.get_user_info_from_token(),
        categories: Category::iter().map(|c| c.to_string()).collect(),
        search: parsed_params
            .title_substring
            .clone()
            .unwrap_or(String::from("")),
        sort_by: String::from(""),
    };

    let body = template.render().map_err(ErrorInternalServerError)?;
    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

#[derive(serde::Deserialize)]
pub struct FilterVideosForm {
    search: SortBy,
    sort: String,
}

#[post("/profile/{id}")]
pub async fn filter_videos(
    path: web::Path<Uuid>,
    user_input: web::Form<FilterVideosForm>,
) -> ActixResult<HttpResponse> {
    let id = path.into_inner();
    let redirect_url = format!(
        "/profile/{}?search={}&sort_by={}",
        id, user_input.search, user_input.sort
    );

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .append_header(("HX-Push-Url", redirect_url))
        .body(""))
}
