use std::ops::Deref;

use actix_web::{
    error::ErrorInternalServerError, get, HttpRequest, HttpResponse, post, Result as ActixResult,
};
use actix_web::cookie::time::OffsetDateTime;
use actix_web::web::{self, Data};
use askama::Template;
use jwt_simple::algorithms::HS256Key;
use jwt_simple::prelude::{Deserialize, Serialize};
use utoipa::ToSchema;

use crate::{templates::LoginTemplate, utils::auth::AuthHandler};
use crate::repositories::user::repository::{PgUserRepository, UserRepository};
use crate::templates::HackedRedirectTemplate;
use crate::utils::auth::{create_jwt, get_login_error_template_response, verify_password};

#[get("/login")]
pub async fn get_login(
    data: HttpRequest,
    key: Data<HS256Key>,
    user_repo: Data<PgUserRepository>,
) -> ActixResult<HttpResponse> {
    let cookies = data.cookies().unwrap().clone();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);

    let template = LoginTemplate {
        already_logged_in: auth.user_logged_in(),
        login_error_message: None,
    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct LoginForm {
    username: String,
    password: String,
}

#[utoipa::path(
responses(
(status = 202, description = "User was authenticated"),
(status = 400, description = "Input params incorrect"),
(status = 403, description = "User not authenticated"),
(status = 501, description = "unknown error"),
)
)]
#[post("/login")]
pub async fn post_login(
    user_repo: Data<PgUserRepository>,
    key: Data<HS256Key>,
    creds: web::Form<LoginForm>,
) -> ActixResult<HttpResponse> {
    let user = user_repo.get_user_by_username(creds.username.clone());
    let user = match user {
        Ok(user) => user,
        Err(_) => return get_login_error_template_response("Invalid Credentials".into()),
    };

    let verify_result = verify_password(user.password_hash.clone(), creds.password.clone());
    if verify_result.is_err() {
        return get_login_error_template_response("Invalid Password".into());
    }

    // AT this point user is verified, return jwt token
    let token = match create_jwt(user.into(), key.get_ref()) {
        Ok(jwt) => jwt,
        Err(_) => {
            return get_login_error_template_response("Internal Server Error".into());
        }
    };

    let cookie = actix_web::cookie::Cookie::build("token", token)
        .path("/")
        .http_only(true)
        .finish();

    let template = HackedRedirectTemplate {
        redirect_url: "/".to_string(),
    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .cookie(cookie)
        .body(body))
}

#[post("/logout")]
pub async fn logout_user(_: HttpRequest) -> ActixResult<HttpResponse> {
    let cookie = actix_web::cookie::Cookie::build("token", "")
        .path("/")
        .expires(OffsetDateTime::UNIX_EPOCH)
        .http_only(true)
        .finish();

    Ok(HttpResponse::SeeOther()
        .content_type("text/html")
        .append_header(("Location", "/"))
        .cookie(cookie)
        .finish())
}
