use actix_web::web::Data;
use actix_web::{
    error::ErrorInternalServerError, get, post, web, HttpRequest, HttpResponse,
    Result as ActixResult,
};
use askama::Template;
use jwt_simple::algorithms::HS256Key;
use serde::{Deserialize, Serialize};
use std::ops::Deref;
use strum::IntoEnumIterator;

use crate::repositories::user::repository::PgUserRepository;
use crate::{
    templates::SubscribedTemplate,
    utils::auth::{AuthHandler, AuthState},
};

use crate::mock::{get_mocked_channels, get_mocked_videos};

use crate::models::Category;
use crate::repositories::channel::models::SelectChannelsManyFilter;
use crate::repositories::channel::repository::{ChannelsRepository, PgChannelRepository};
use crate::repositories::video::models::{SelectVideoManyFilter, SortBy};
use crate::repositories::video::repository::{PgVideoRepository, VideoRepository};
use crate::templates::{ChannelsListTemplate, ChannelsVideosTemplate};
use crate::utils::params_mapper::{map_sort_by_string, map_sort_by_to_string};

#[derive(Serialize, Deserialize)]
struct SubscribedParams {
    search: Option<String>,
    sort_by: Option<String>,
    channel_search: Option<String>,
}

#[get("/subscribed")]
pub async fn get_subscribed(
    data: HttpRequest,
    video_repo: Data<PgVideoRepository>,
    channel_repo: Data<PgChannelRepository>,
    user_repo: Data<PgUserRepository>,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let cookies = data.cookies().map(|c| c.to_owned()).unwrap_or_default();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let user_info = auth.get_user_info_from_token();

    let parsed_params = web::Query::<SubscribedParams>::from_query(data.query_string()).unwrap();
    let sort_by = map_sort_by_string(
        parsed_params
            .sort_by
            .clone()
            .unwrap_or("newest".to_string()),
    );

    let mut video_filter = SelectVideoManyFilter::new();
    video_filter.title_substring = parsed_params.search.clone();
    video_filter.include_premium = Some(auth.is_user_premium());
    video_filter.sort_by = sort_by.clone();
    let mut videos = video_repo.get_videos(video_filter).unwrap_or_default();
    videos.reverse();

    let template = SubscribedTemplate {
        already_logged_in: auth.get_token_from_cookie() != AuthState::LoggedOut,
        categories: Category::iter().map(|c| c.to_string()).collect(),
        user_info: user_info.clone(),
        title_substring: parsed_params.search.clone().unwrap_or(String::from("")),
        sort_by: map_sort_by_to_string(sort_by.unwrap_or(SortBy::Newest)),
        channel_search: parsed_params
            .channel_search
            .clone()
            .unwrap_or("".to_string())
            .to_string(),
        videos,
        channels: channel_repo
            .get_channels(SelectChannelsManyFilter::new(parsed_params.search.clone()))?,
    };
    let body = template.render().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

#[post("/subscribed/channels")]
pub async fn post_search_channels(
    user_input: web::Form<SelectChannelsManyFilter>,
    data: HttpRequest,
) -> ActixResult<HttpResponse> {
    let parsed_params = web::Query::<SubscribedParams>::from_query(data.query_string()).unwrap();

    let mut channels = get_mocked_channels();
    channels.reverse();

    let template = ChannelsListTemplate {
        channels,
        channel_search: user_input.search.clone().unwrap_or(String::from("")),
    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    let redirect_url = format!(
        "/subscribed?channel_search={}&search={}&sort_by={}",
        user_input.search.clone().unwrap_or(String::from("")),
        parsed_params.search.clone().unwrap_or(String::from("")),
        parsed_params
            .sort_by
            .clone()
            .unwrap_or("newest".to_string())
    );

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .append_header(("HX-Push-Url", redirect_url))
        .body(body))
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct ChannelVideoSearch {
    pub search: Option<String>,
    pub sort: Option<String>,
}

#[post("/subscribed/videos")]
pub async fn post_search_subscribed_videos(
    user_input: web::Form<ChannelVideoSearch>,
    data: HttpRequest,
) -> ActixResult<HttpResponse> {
    let parsed_params = web::Query::<SubscribedParams>::from_query(data.query_string()).unwrap();

    let mut videos = get_mocked_videos();
    videos.reverse();

    let template = ChannelsVideosTemplate {
        title_substring: user_input.search.clone().unwrap_or(String::from("")),
        sort_by: user_input.sort.clone().unwrap_or(String::from("Newest")),
        videos,
    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    let redirect_url = format!(
        "/subscribed?channel_search={}&search={}&sort_by={}",
        parsed_params.search.clone().unwrap_or(String::from("")),
        user_input.search.clone().unwrap_or(String::from("")),
        user_input.sort.clone().unwrap_or(String::from("newest"))
    );

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .append_header(("HX-Push-Url", redirect_url))
        .body(body))
}
