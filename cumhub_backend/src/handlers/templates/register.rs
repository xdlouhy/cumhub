use crate::repositories::user::repository::{PgUserRepository, UserRepository};
use crate::templates::{HackedRedirectTemplate, RegisterTemplate};
use crate::utils::auth::{hash_password, AuthHandler};
use actix_web::web::{Data, Form};
use actix_web::{
    error::ErrorInternalServerError, get, post, HttpRequest, HttpResponse, Result as ActixResult,
};
use askama::Template;
use jwt_simple::algorithms::HS256Key;
use jwt_simple::prelude::Serialize;
use serde::Deserialize;
use std::ops::Deref;

#[get("/register")]
pub async fn get_register(
    data: HttpRequest,
    user_repo: Data<PgUserRepository>,
    key: Data<HS256Key>,
) -> ActixResult<HttpResponse> {
    let cookies = data.cookies().unwrap().clone();
    let auth = AuthHandler::new(cookies, key.deref(), &user_repo);
    let _ = auth.get_token_from_cookie();
    let _ = auth.get_user_info_from_token();

    let template = RegisterTemplate {
        already_logged_in: auth.user_logged_in(),
    };

    let body = template.render().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}

#[derive(Serialize, Deserialize)]
pub struct RegisterParams {
    username: String,
    email: String,
    profile_pic: String,
    password: String,
}
#[post("/register")]
pub async fn post_register(
    user: Form<RegisterParams>,
    data: Data<PgUserRepository>,
) -> ActixResult<HttpResponse> {
    let form = user.into_inner();

    let email_taken = data.get_user_by_email(form.email.clone());
    if email_taken.is_ok() {
        return Ok(HttpResponse::BadRequest().body("Email already taken".to_string()));
    }

    let username_taken = data.get_user_by_username(form.username.clone());
    if username_taken.is_ok() {
        return Ok(HttpResponse::BadRequest().body("Username already taken".to_string()));
    }

    let hash_result = hash_password(form.password.clone());
    let _ = match hash_result {
        Ok(hash) => hash,
        Err(_) => {
            return Ok(
                HttpResponse::InternalServerError().body("Error hashing password".to_string())
            )
        }
    };

    let redirect = HackedRedirectTemplate {
        redirect_url: "/login".to_string(),
    };

    let body = redirect.render().map_err(ErrorInternalServerError)?;

    Ok(HttpResponse::Ok().content_type("text/html").body(body))
}
