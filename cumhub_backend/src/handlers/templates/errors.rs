use actix_web::{error::ErrorInternalServerError, get, HttpRequest, HttpResponse, Result as ActixResult};
use askama::Template;
use strum::IntoEnumIterator;

use crate::models::Category;
use crate::templates::{AccessDeniedTemplate, BadRequestTemplate, NotFoundTemplate};

#[get("/access_denied")]
pub async fn access_denied(
    _: HttpRequest
) -> ActixResult<HttpResponse> {

    let template = AccessDeniedTemplate {};
    let body = template.render().map_err(ErrorInternalServerError)?;
    Ok(HttpResponse::Forbidden().content_type("text/html").body(body))
}

#[get("/not_found")]
pub async fn get_page_not_found() -> ActixResult<HttpResponse> {
    page_not_found().await
}

pub async fn page_not_found() -> ActixResult<HttpResponse> {
    let template = NotFoundTemplate {
        user_info: None,
        categories: Category::iter().map(|c| c.to_string()).collect(),
    };
    let body = template.render().map_err(ErrorInternalServerError)?;
    Ok(HttpResponse::NotFound().content_type("text/html").body(body))
}

pub async fn bad_request_page(message: String) -> ActixResult<HttpResponse> {
    let template = BadRequestTemplate {
        message,
        user_info: None,
        categories: Category::iter().map(|c| c.to_string()).collect(),
    };
    let body = template.render().map_err(ErrorInternalServerError)?;
    Ok(HttpResponse::BadRequest().content_type("text/html").body(body))
}