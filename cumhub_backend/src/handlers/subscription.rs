use actix_web::{delete, get, HttpResponse, post, Responder, web};
use actix_web::web::{Data, Path, Query};
use uuid::Uuid;
use web::Json;

use crate::errors::ApiError;
use crate::repositories::subscription::models::SubscriptionId;
use crate::repositories::subscription::repository::{PgSubscriptionRepository, SubscriptionRepository};

#[utoipa::path(
request_body = SubscriptionId,
responses(
(status = 201, description = "Saves new subscription to database!"),
(status = 400, description = "Specified subscription cannot be created!")
)
)]
#[post("/api/subscription")]
pub async fn create_subscription(
    subscription_repo: Data<PgSubscriptionRepository>,
    create_subscription: Json<SubscriptionId>,
) -> impl Responder {
    let new_subscription = subscription_repo.create_subscription(create_subscription.0);
    match new_subscription {
        Ok(subscription) => HttpResponse::Created().json(subscription),
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
params(),
responses(
(status = 200, description = "Retrieves filtered subscriptions!"),
(status = 204, description = "No subscriptions matched filters"),
(status = 400, description = "Specified subscriptions cannot be fetched!")
)
)]
#[get("/api/subscriptions/{id}")]
pub async fn get_subscriptions(
    subscription_repo: Data<PgSubscriptionRepository>,
    user_id: Path<Uuid>
) -> impl Responder {
    let obtained_subscriptions = subscription_repo.get_subscriptions(user_id.into_inner());

    match obtained_subscriptions {
        Ok(subscriptions) => {
            if subscriptions.is_empty() {
                return HttpResponse::NoContent().body(());
            }
            HttpResponse::Ok().json(subscriptions)
        }
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
responses(
(status = 204, description = "Subscription was deleted"),
(status = 400, description = "Specified subscription cannot be deleted!")
)
)]
#[delete("/api/subscription/{id}")]
pub async fn delete_subscription(
    subscription_repo: Data<PgSubscriptionRepository>,
    subscription_id: Query<SubscriptionId>,
) -> impl Responder {
    let deleted_subscription = subscription_repo.delete_subscription(subscription_id.into_inner());
    match deleted_subscription {
        Ok(_subscription) => HttpResponse::NoContent().body(()),
        Err(err) => ApiError::from(err).into(),
    }
}