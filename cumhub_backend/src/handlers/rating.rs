use actix_web::{delete, get, HttpResponse, post, Responder, web};
use actix_web::web::{Data, Query};
use web::Json;

use crate::errors::ApiError;
use crate::repositories::rating::models::{NewRating, RatingId, SelectRatingsManyFilter};
use crate::repositories::rating::repository::{PgRatingRepository, RatingRepository};

#[utoipa::path(
request_body = NewRating,
responses(
(status = 201, description = "Saves new rating to database!"),
(status = 400, description = "Specified rating cannot be created!")
)
)]
#[post("/api/rating")]
pub async fn create_rating(
    rating_repo: Data<PgRatingRepository>,
    create_rating: Json<NewRating>,
) -> impl Responder {
    let new_rating = rating_repo.create_rating(create_rating.0);
    match new_rating {
        Ok(rating) => HttpResponse::Created().json(rating),
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
params(SelectRatingsManyFilter),
responses(
(status = 200, description = "Retrieves filtered ratings!"),
(status = 204, description = "No ratings matched filters"),
(status = 400, description = "Specified ratings cannot be fetched!")
)
)]
#[get("/api/ratings")]
pub async fn get_ratings(
    rating_repo: Data<PgRatingRepository>,
    filter: Query<SelectRatingsManyFilter>,
) -> impl Responder {
    let obtained_ratings = rating_repo.get_ratings(filter.0);

    match obtained_ratings {
        Ok(ratings) => {
            if ratings.is_empty() {
                return HttpResponse::NoContent().body(());
            }
            HttpResponse::Ok().json(ratings)
        }
        Err(err) => ApiError::from(err).into(),
    }
}

#[utoipa::path(
responses(
(status = 204, description = "Rating was deleted"),
(status = 400, description = "Specified comment cannot be deleted!")
)
)]
#[delete("/api/rating/{id}")]
pub async fn delete_rating(
    rating_repo: Data<PgRatingRepository>,
    rating_id: Query<RatingId>,
) -> impl Responder {
    let deleted_rating = rating_repo.delete_rating(rating_id.into_inner());
    match deleted_rating {
        Ok(_rating) => HttpResponse::NoContent().body(()),
        Err(err) => ApiError::from(err).into(),
    }
}