use actix_web::{HttpResponse, ResponseError};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use utoipa::ToSchema;

/// Internal database layer error type
#[derive(Error, Debug)]
pub enum DatabaseError {
    #[error("database error: {0}")]
    Error(diesel::result::Error),
    #[error("connection pool error: {0}")]
    ConnectionError(#[from] r2d2::Error),
    #[error("broken constraint")]
    BrokenConstraint,
    #[error("no record found")]
    NotFound,
}

impl ResponseError for DatabaseError {
    fn status_code(&self) -> actix_web::http::StatusCode {
        match self {
            DatabaseError::Error(_) => actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            DatabaseError::ConnectionError(_) => actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
            DatabaseError::BrokenConstraint => actix_web::http::StatusCode::BAD_REQUEST,
            DatabaseError::NotFound => actix_web::http::StatusCode::NOT_FOUND,
        }
    }

    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code()).body(format!("Database Error: {:?}", self))
    }
}


impl From<diesel::result::Error> for DatabaseError {
    fn from(error: diesel::result::Error) -> Self {
        match error {
            diesel::result::Error::NotFound => DatabaseError::NotFound,
            err => DatabaseError::Error(err),
        }
    }
}

/// User facing error type
#[derive(Error, Debug, Serialize, ToSchema)]
pub enum ApiError {
    #[error("database error: {0}")]
    InternalServerError(String),
    #[error("not found")]
    NotFound,
    #[error("bad request")]
    BadRequest,
}

impl From<DatabaseError> for ApiError {
    fn from(error: DatabaseError) -> Self {
        match error {
            DatabaseError::ConnectionError(ex)  => {
                Self::InternalServerError(ex.to_string())
            }
            DatabaseError::Error(ex) => {
                Self::InternalServerError(ex.to_string())
            }
            DatabaseError::BrokenConstraint => {
                Self::BadRequest
            }
            DatabaseError::NotFound => {
                Self::NotFound
            }
        }
    }
}

impl From<ApiError> for HttpResponse {
    fn from(val: ApiError) -> Self {
        match val {
            ApiError::InternalServerError(_) => HttpResponse::InternalServerError(),
            ApiError::NotFound => HttpResponse::NotFound(),
            ApiError::BadRequest => HttpResponse::BadRequest(),
        }
            .json(Error {
                error: val.to_string(),
            })
    }
}

#[derive(Serialize, Deserialize)]
struct Error {
    error: String,
}

