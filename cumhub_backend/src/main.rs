use std::env;

use actix_web::{App, HttpServer};
use actix_web::middleware::Logger;
use actix_web::web::Data;
use diesel::PgConnection;
use diesel::r2d2::ConnectionManager;
use env_logger::Env;
use log::{info, warn};
use r2d2::Pool;
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

use models::*;

use crate::handlers::user::NewUserRequest;

use crate::errors::ApiError;
use crate::handlers::templates::login::LoginForm;
use crate::repositories::channel::repository::PgChannelRepository;
use crate::repositories::comment::models::*;
use crate::repositories::comment::repository::PgCommentRepository;
use crate::repositories::common::Pagination;
use crate::repositories::rating::models::*;
use crate::repositories::rating::repository::PgRatingRepository;
use crate::repositories::subscription::repository::PgSubscriptionRepository;
use crate::repositories::user::models::*;
use crate::repositories::channel::models::*;
use crate::repositories::user::repository::PgUserRepository;
use crate::repositories::video::models::*;
use crate::repositories::video::repository::PgVideoRepository;

use crate::handlers::video::VideoFormData;
use crate::utils::auth::generate_jwt_key;

mod init;
mod repositories;
mod models;
mod schema;
mod errors;
mod handlers;
mod templates;
mod utils;
mod mock;

const DEFAULT_HOSTNAME: &str = "localhost";
const DEFAULT_PORT: &str = "6969";

const DEFAULT_DB_POOL_SIZE: &str = "20";

const DEFAULT_WORKER_COUNT: &str = "4";

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init_from_env(Env::default().default_filter_or("info"));

    if let Err(e) = dotenv::dotenv() {
        warn!("failed loading .env file: {e}")
    };

    let host = parse_host();
    info!("starting server on {host}");


    #[derive(OpenApi)]
    #[openapi(
    paths(
    handlers::comment::create_comment,
    handlers::comment::get_comment,
    handlers::comment::get_comments,
    handlers::comment::edit_comment,
    handlers::comment::delete_comment,
    handlers::rating::create_rating,
    handlers::rating::get_ratings,
    handlers::rating::delete_rating,
    handlers::user::create_user,
    handlers::user::get_user,
    handlers::user::get_user_by_username,
    handlers::user::edit_user,
    handlers::user::delete_user,
    handlers::video::post_video,
    handlers::video::get_video,
    handlers::video::get_videos,
    handlers::video::delete_video,
    handlers::channel::get_channel,
    handlers::channel::get_channels,
    handlers::templates::login::post_login,
    utils::auth::try_token
    ),
    components(
    schemas(
    ApiError, LoginForm, Pagination,
    Comment, NewComment, PartialComment, CommentWithOtherData,
    Rating, NewRating, RatingId,
    User, NewUserRequest, PartialUser, PartialUser,
    Video, NewVideo, SelectVideoManyFilter, VideoWithOtherData,
    SelectChannelsManyFilter, ChannelInfo, SortBy, Category, VideoFormData
    )
    ),
    tags(
    (name = "CumHub", description = "CumHub endpoints.")
    ),
    )]
    struct ApiDoc;

    let openapi = ApiDoc::openapi();

    let pool = set_up_database_pool();
    let key = generate_jwt_key();

    let _ = HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .configure(init::configure_webapp)
            .app_data(Data::new(PgChannelRepository::new(pool.clone())))
            .app_data(Data::new(PgUserRepository::new(pool.clone())))
            .app_data(Data::new(PgVideoRepository::new(pool.clone())))
            .app_data(Data::new(PgCommentRepository::new(pool.clone())))
            .app_data(Data::new(PgRatingRepository::new(pool.clone())))
            .app_data(Data::new(PgSubscriptionRepository::new(pool.clone())))

            .app_data(Data::new(key.clone()))
            .service(
                SwaggerUi::new("/swagger-ui/{_:.*}").url("/api-doc/openapi.json", openapi.clone()),
            )
    })
        .workers(
            env::var("WORKER_COUNT")
                .unwrap_or(DEFAULT_WORKER_COUNT.to_string())
                .parse::<usize>()
                .expect("Failed parse worker count value!"))
        .bind(host)?
        .run()
        .await;

    Ok(())
}

fn parse_host() -> String {
    let hostname = env::var("HOSTNAME").unwrap_or(DEFAULT_HOSTNAME.to_string());
    let port = env::var("PORT").unwrap_or(DEFAULT_PORT.to_string());
    format!("{hostname}:{port}")
}

fn set_up_database_pool() -> Pool<ConnectionManager<PgConnection>> {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let manager = ConnectionManager::<PgConnection>::new(database_url);
     Pool::builder()
        .test_on_check_out(true)
        .max_size(
            env::var("DATABASE_POOL_SIZE")
                .unwrap_or(DEFAULT_DB_POOL_SIZE.to_string())
                .parse::<u32>()
                .expect("Failed parse database pool size value!"))
        .build(manager)
        .expect("Failed setting up database pool")
}