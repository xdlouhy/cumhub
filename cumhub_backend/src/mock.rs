// Just for developing FE

use chrono::Utc;
use strum::IntoEnumIterator;
use uuid::Uuid;

use crate::{
    models::{Category, CommentWithOtherData, VideoWithOtherData},
    templates::{ProfileTemplate, UserInfo},
};

use crate::models::ChannelInfo;

pub fn get_mocked_videos() -> Vec<VideoWithOtherData> {
    vec![
        VideoWithOtherData {
            id: Uuid::new_v4(),
            author_id: Uuid::new_v4(),
            author_username: "pepa".to_string(),
            categories: vec![crate::models::Category::Bdsm],
            deleted_at: None,
            is_premium: false,
            views: 501,
            description: Some("Piss and cum".to_string()),
            length: 420,
            title: "Piss and cum".to_string(),
            url: "".to_string(),
            uploaded_at: chrono::Utc::now(),
            likes: 100,
        },
        VideoWithOtherData {
            id: Uuid::new_v4(),
            author_id: Uuid::new_v4(),
            author_username: "kokodak".to_string(),
            views: 1000,
            categories: vec![crate::models::Category::Bdsm],
            deleted_at: None,
            is_premium: false,
            description: Some("Two girls one cum".to_string()),
            length: 69,
            title: "Two girls one cum".to_string(),
            url: "".to_string(),
            uploaded_at: chrono::Utc::now(),
            likes: 0,
        },
    ]
}

pub fn get_mocked_profile_template(
    logged_in: bool,
    search: String,
    sort_by: String,
) -> ProfileTemplate {
    let videos = get_mocked_videos();
    let user_info = UserInfo {
        id: Uuid::new_v4(),
        username: String::from("test"),
        email: String::from("testtest@test.test"),
        description: Some("aaaaaa".to_string()),
        profile_picture: None,
        premium_expiration: Some(chrono::Utc::now()),
        has_premium: true,
    };

    ProfileTemplate {
        profile_name: "CumQueen".to_string(),
        profile_description: " Content Creator \n
        | Passionate about Body Fluid Exchange 🤝\n
        | Unraveling the mysteries within every drop 💧"
            .to_string(),
        subscribers: 420690,
        videos,
        user_subscribed: true,
        already_logged_in: logged_in,
        user_info: Some(user_info),
        search,
        sort_by,
        categories: Category::iter().map(|c| c.to_string()).collect(),
        channel_pic_url: None,
    }
}

pub fn get_mocked_comments() -> Vec<CommentWithOtherData> {
    let video_id = Uuid::new_v4();

    // Mock comments with additional data
    let comment1 = CommentWithOtherData {
        id: Uuid::new_v4(),
        author_id: Uuid::new_v4(),
        author_username: "Alice".to_string(),
        video_id,
        content: "Great video!".to_string(),
        created_at: Utc::now(),
        deleted_at: None,
    };

    let comment2 = CommentWithOtherData {
        id: Uuid::new_v4(),
        author_id: Uuid::new_v4(),
        author_username: "Bob".to_string(),
        video_id,
        content: "Very informative, thanks!".to_string(),
        created_at: Utc::now(),
        deleted_at: None,
    };

    let comment3 = CommentWithOtherData {
        id: Uuid::new_v4(),
        author_id: Uuid::new_v4(),
        author_username: "Charlie".to_string(),
        video_id,
        content: "I enjoyed watching this.".to_string(),
        created_at: Utc::now(),
        deleted_at: None,
    };

    vec![comment1, comment2, comment3]
}

fn get_mocked_video(video_id: Uuid) -> VideoWithOtherData {
    VideoWithOtherData {
        id: video_id,
        author_id: Uuid::new_v4(),
        deleted_at: None,
        is_premium: false,
        description: Some("Two girls one cum".to_string()),
        length: 69,
        title: "Two girls one cum".to_string(),
        url: "".to_string(),
        uploaded_at: chrono::Utc::now(),
        author_username: "OhYeahUser".to_string(),
        views: 112_221,
        categories: vec![crate::models::Category::Bdsm],
        likes: 0,
    }
}

pub fn get_mocked_channels() -> Vec<ChannelInfo> {
    vec![
        ChannelInfo {
            id: Uuid::new_v4(),
            username: "MidgetSpinner".to_string(),
            profile_picture: Some(
                "https://wallpapers-clan.com/wp-content/uploads/2022/08/default-pfp-36.jpg"
                    .to_string(),
            ),
            subscribers: 420,
            description: Some(String::from("aaa")),
        },
        ChannelInfo {
            id: Uuid::new_v4(),
            username: "UnknownCock".to_string(),
            profile_picture: Some(
                "https://wallpapers-clan.com/wp-content/uploads/2022/08/default-pfp-27.jpg"
                    .to_string(),
            ),
            subscribers: 843010,
            description: Some(String::from("aaa")),
        },
        ChannelInfo {
            id: Uuid::new_v4(),
            username: "LickMaster420".to_string(),
            profile_picture: Some(
                "https://wallpapers-clan.com/wp-content/uploads/2022/08/default-pfp-37.jpg"
                    .to_string(),
            ),
            subscribers: 1433010,
            description: Some(String::from("aaa")),
        },
        ChannelInfo {
            id: Uuid::new_v4(),
            username: "ShitLover".to_string(),
            profile_picture: Some(
                "https://wallpapers-clan.com/wp-content/uploads/2022/08/default-pfp-38.jpg"
                    .to_string(),
            ),
            subscribers: 123,
            description: Some(String::from("aaa")),
        },
        ChannelInfo {
            id: Uuid::new_v4(),
            username: "FeetEnjoyer42069".to_string(),
            profile_picture: Some(
                "https://wallpapers-clan.com/wp-content/uploads/2022/08/default-pfp-26.jpg"
                    .to_string(),
            ),
            subscribers: 16,
            description: Some(String::from("aaa")),
        },
        ChannelInfo {
            id: Uuid::new_v4(),
            username: "IDK".to_string(),
            profile_picture: Some(
                "https://wallpapers-clan.com/wp-content/uploads/2022/08/default-pfp-22.jpg"
                    .to_string(),
            ),
            subscribers: 16,
            description: Some(String::from("aaa")),
        },
    ]
}
