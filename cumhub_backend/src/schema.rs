// @generated automatically by Diesel CLI.

diesel::table! {
    comment_details_view {
        id -> Uuid,
        author_id -> Uuid,
        #[max_length = 255]
        author_username -> Varchar,
        video_id -> Uuid,
        #[max_length = 255]
        content -> Varchar,
        created_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    channel_info {
        id -> Uuid,
        #[max_length = 255]
        username -> Varchar,
        #[max_length = 255]
        description -> Nullable<Varchar>,
        #[max_length = 255]
        profile_picture -> Nullable<Varchar>,
        subscribers -> Int8
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Category;

    video_details_view {
        id -> Uuid,
        author_id -> Uuid,
        #[max_length = 255]
        author_username -> Varchar,
        #[max_length = 255]
        title -> Varchar,
        #[max_length = 255]
        description -> Nullable<Varchar>,
        #[max_length = 255]
        url -> Varchar,
        views -> Int4,
        length -> Int4,
        is_premium -> Bool,
        uploaded_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
        categories -> Array<Category>,
        likes -> Int8,
    }
}

pub mod sql_types {
    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "category"))]
    pub struct Category;
}

diesel::table! {
    comment (id) {
        id -> Uuid,
        author_id -> Uuid,
        video_id -> Uuid,
        #[max_length = 255]
        content -> Varchar,
        created_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    rating (author_id, video_id) {
        author_id -> Uuid,
        video_id -> Uuid,
        liked -> Bool,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    subscription (subscriber_id, author_id) {
        subscriber_id -> Uuid,
        author_id -> Uuid,
        created_at -> Timestamptz,
    }
}

diesel::table! {
    user (id) {
        id -> Uuid,
        #[max_length = 255]
        username -> Nullable<Varchar>,
        #[max_length = 255]
        email -> Nullable<Varchar>,
        #[max_length = 255]
        description -> Nullable<Varchar>,
        #[max_length = 255]
        profile_pic_url -> Nullable<Varchar>,
        premium_expiration -> Nullable<Timestamptz>,
        #[max_length = 255]
        password_hash -> Varchar,
        created_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    video (id) {
        id -> Uuid,
        author_id -> Uuid,
        #[max_length = 255]
        title -> Varchar,
        #[max_length = 255]
        description -> Nullable<Varchar>,
        #[max_length = 255]
        url -> Varchar,
        views -> Int4,
        length -> Int4,
        is_premium -> Bool,
        uploaded_at -> Timestamptz,
        deleted_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::Category;

    video_categories (video_id, category) {
        video_id -> Uuid,
        category -> Category,
    }
}

diesel::joinable!(comment -> user (author_id));
diesel::joinable!(comment -> video (video_id));
diesel::joinable!(rating -> user (author_id));
diesel::joinable!(rating -> video (video_id));
diesel::joinable!(video -> user (author_id));
diesel::joinable!(video_categories -> video (video_id));

diesel::allow_tables_to_appear_in_same_query!(
    comment,
    rating,
    subscription,
    user,
    video,
    video_categories,
);