use std::env;
use std::sync::Once;

use actix_files::Files as ActixFiles;
use actix_web::web;
use actix_web::web::{Data, ServiceConfig};
use diesel::{Connection, PgConnection};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use log::warn;

use crate::handlers::channel::{get_channel, get_channels};
use crate::handlers::comment::{
    create_comment, delete_comment, edit_comment, get_comment, get_comments,
};

use crate::handlers::rating::{create_rating, delete_rating, get_ratings};
use crate::handlers::subscription::{create_subscription, delete_subscription, get_subscriptions};
use crate::handlers::templates::account_settings::{
    get_account_settings, patch_account_description, patch_account_password,
    patch_account_profile_pic, patch_account_username,
};
use crate::handlers::templates::errors::{access_denied, page_not_found};
use crate::handlers::templates::index::filter_index_videos;
use crate::handlers::templates::login::{get_login, logout_user, post_login};
use crate::handlers::templates::premium::{get_premium, post_premium_subscribe};
use crate::handlers::templates::profile::filter_videos;
use crate::handlers::templates::register::{get_register, post_register};
use crate::handlers::templates::subscribed::{
    get_subscribed, post_search_channels, post_search_subscribed_videos,
};
use crate::handlers::templates::upload::{get_upload, upload_video};
use crate::handlers::templates::video::{
    post_comment_to_video, post_dislike_to_video, post_like_to_video, post_rating_to_video,
};
use crate::handlers::templates::{index, profile, video};

use crate::handlers::user::{
    buy_premium, create_user, delete_user, edit_user, get_user, get_user_by_username,
};
use crate::handlers::video::{delete_video, get_video, get_videos, post_video};
use crate::utils::auth::try_token;

const MIGRATIONS: EmbeddedMigrations = embed_migrations!();
static INITIAL_MIGRATION: Once = Once::new();

pub fn configure_webapp(_config: &mut ServiceConfig) {
    if let Err(e) = dotenv::dotenv() {
        warn!("failed loading .env file: {e}")
    };
    INITIAL_MIGRATION.call_once(run_migrations);

    let _client = awc::Client::new();
    _config
        .service(index::get_index)
        .service(profile::get_profile)
        .service(get_login)
        .service(filter_index_videos)
        .service(video::get_video)
        .service(post_comment_to_video)
        .service(post_like_to_video)
        .service(post_dislike_to_video)
        .service(ActixFiles::new("/static/", "./src/static").prefer_utf8(true));

    _config
        .service(create_comment)
        .service(get_comment)
        .service(get_comments)
        .service(edit_comment)
        .service(delete_comment)
        .service(create_rating)
        .service(get_ratings)
        .service(delete_rating)
        .service(create_user)
        .service(get_user)
        .service(buy_premium)
        .service(post_search_subscribed_videos)
        .service(get_user_by_username)
        .service(delete_user)
        .service(edit_user)
        .service(post_video)
        .service(get_video)
        .service(get_videos)
        .service(delete_video)
        .service(filter_videos)
        .service(get_login)
        .service(filter_index_videos)
        .service(create_subscription)
        .service(get_subscriptions)
        .service(delete_subscription)
        .service(get_upload)
        .service(get_account_settings)
        .service(patch_account_password)
        .service(patch_account_username)
        .service(patch_account_description)
        .service(patch_account_profile_pic)
        .service(upload_video)
        .service(get_register)
        .service(post_register)
        .service(get_subscribed)
        .service(post_search_channels)
        .service(get_channel)
        .service(post_premium_subscribe)
        .service(get_channels)
        .service(get_premium)
        .service(post_login)
        .service(logout_user)
        .app_data(Data::new(_client.clone()));

    _config.service(try_token);

    _config
        .service(access_denied)
        .default_service(web::route().to(page_not_found));
}

pub fn run_migrations() {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let mut connection = PgConnection::establish(&database_url)
        .expect("failed establishing connection for migrations");
    connection
        .run_pending_migrations(MIGRATIONS)
        .expect("failed migrating database");
}
