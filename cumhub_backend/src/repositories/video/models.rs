use crate::models::Category;
use crate::repositories::common::Pagination;
use diesel::{AsChangeset, Insertable};
use serde::{Deserialize, Serialize};
use strum_macros::{Display, EnumIter, EnumString};
use utoipa::{IntoParams, ToSchema};
use uuid::Uuid;

#[derive(
    Serialize, Deserialize, Debug, Clone, PartialEq, ToSchema, EnumString, Display, EnumIter,
)]
pub enum SortBy {
    Newest,
    Oldest,
    MostLiked,
    MostDisliked,
    MostViewed,
}

#[derive(Serialize, Deserialize, Debug, ToSchema, IntoParams, Default, Clone)]
pub struct SelectVideoManyFilter {
    pub author_ids: Option<Vec<Uuid>>,
    pub title_substring: Option<String>,
    pub category: Option<Category>,
    pub length_from: Option<i32>,
    pub length_to: Option<i32>,
    pub include_premium: Option<bool>,
    pub pagination: Option<Pagination>,
    pub sort_by: Option<SortBy>,
}

#[derive(Serialize, Deserialize, Debug, IntoParams, Default, Clone)]
pub struct SelectVideoManyFilterRaw {
    pub author_ids: Option<Vec<Uuid>>,
    pub title_substring: Option<String>,
    pub category: Option<String>,
    pub length_from: Option<i32>,
    pub length_to: Option<i32>,
    pub include_premium: Option<bool>,
    pub limit: Option<i64>,
    pub offset: Option<i64>,
    pub sort_by: Option<String>,
}

impl SelectVideoManyFilter {
    pub fn new() -> Self {
        Self {
            author_ids: None,
            title_substring: None,
            category: None,
            length_from: None,
            length_to: None,
            include_premium: None,
            pagination: None,
            sort_by: None,
        }
    }

    pub fn by_author_id(id: Uuid) -> Self {
        let mut filter = Self::new();
        filter.author_ids = Some(vec![id]);
        filter
    }
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, ToSchema, Clone)]
#[diesel(table_name = crate::schema::video)]
pub struct PartialVideo {
    pub title: Option<String>,
    pub description: Option<String>,
    pub is_premium: Option<bool>,
}

#[derive(Serialize, Deserialize, Clone, ToSchema)]
pub struct NewVideo {
    pub author_id: Uuid,
    pub title: String,
    pub description: String,
    pub categories: Vec<Category>,
    pub url: String,
    pub length: i32,
    pub is_premium: bool,
}
