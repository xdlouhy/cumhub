use std::ops::DerefMut;

use diesel::{
    AsChangeset, Connection, delete, ExpressionMethods, Insertable, PgArrayExpressionMethods,
    QueryDsl, RunQueryDsl, SelectableHelper, TextExpressionMethods,
};
use diesel::pg::*;
use diesel::r2d2::{ConnectionManager, Pool};
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;
use uuid::Uuid;

use crate::errors::DatabaseError;
use crate::models::{Video, VideoCategory, VideoWithOtherData};
use crate::repositories::video::models::{NewVideo, PartialVideo, SelectVideoManyFilter, SortBy};
use crate::schema;
use crate::schema::video::id;
use crate::schema::video_details_view::{
    author_id, categories, deleted_at, is_premium, length, title,
};

pub trait VideoRepository {
    fn get_video(&self, video_id: Uuid) -> Result<VideoWithOtherData, DatabaseError>;
    fn get_all_videos(&self) -> Result<Vec<VideoWithOtherData>, DatabaseError>;
    fn get_videos(
        &self,
        filters: SelectVideoManyFilter,
    ) -> Result<Vec<VideoWithOtherData>, DatabaseError>;
    fn delete_video(&self, video_id: Uuid) -> Result<(), DatabaseError>;
    fn create_video(&self, new_video: NewVideo) -> Result<Video, DatabaseError>;

    fn edit_video(&self, video_id: Uuid, patch_video: PartialVideo)
        -> Result<Video, DatabaseError>;
    fn increment_view_count(&self, video_id: Uuid) -> Result<(), DatabaseError>;
}

#[derive(Clone)]
pub struct PgVideoRepository {
    pg_pool: Pool<ConnectionManager<PgConnection>>,
}

impl PgVideoRepository {
    pub fn new(pg_pool: Pool<ConnectionManager<PgConnection>>) -> Self {
        Self { pg_pool }
    }
}

impl VideoRepository for PgVideoRepository {
    fn get_video(&self, video_id_value: Uuid) -> Result<VideoWithOtherData, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let video = schema::video_details_view::dsl::video_details_view
            .find(video_id_value)
            .filter(deleted_at.is_null())
            .select(VideoWithOtherData::as_select())
            .first(conn.deref_mut())?;

        Ok(video)
    }

    fn get_all_videos(&self) -> Result<Vec<VideoWithOtherData>, DatabaseError> {
        let query = schema::video_details_view::dsl::video_details_view
            .filter(deleted_at.is_null())
            .order(schema::video_details_view::dsl::uploaded_at.desc())
            .into_boxed();

        let mut conn = self.pg_pool.get()?;
        let videos = query.load::<VideoWithOtherData>(conn.deref_mut())?;

        Ok(videos)
    }

    fn get_videos(
        &self,
        filters: SelectVideoManyFilter,
    ) -> Result<Vec<VideoWithOtherData>, DatabaseError> {
        let mut query = schema::video_details_view::dsl::video_details_view
            .filter(deleted_at.is_null())
            .into_boxed();

        query = match filters.sort_by {
            None | Some(SortBy::Newest) => query.order(schema::video_details_view::dsl::uploaded_at.desc()),
            Some(SortBy::Oldest) => query.order(schema::video_details_view::dsl::uploaded_at.asc()),
            Some(SortBy::MostViewed) => query.order(schema::video_details_view::dsl::views),
            Some(SortBy::MostLiked) => query.order(schema::video_details_view::dsl::likes.desc()),
            Some(SortBy::MostDisliked) => query.order(schema::video_details_view::dsl::likes.asc()),
        };

        if let Some(filter_users) = filters.author_ids {
            query = query.filter(author_id.eq_any(filter_users));
        }

        if let Some(filter_title_substring) = filters.title_substring {
            query = query.filter(title.like(format!("%{}%", filter_title_substring)));
        }

        if let Some(filter_category) = filters.category {
            query = query.filter(categories.contains(vec![filter_category]));
        }

        if let Some(length_from) = filters.length_from {
            query = query.filter(length.ge(length_from));
        }

        if let Some(length_to) = filters.length_to {
            query = query.filter(length.le(length_to));
        }

        if !filters.include_premium.unwrap_or(false) {
            query = query.filter(is_premium.eq(false));
        }

        if let Some(pagination) = filters.pagination {
            query = query.limit(pagination.limit.unwrap_or(i64::MAX));
            query = query.offset(pagination.offset.unwrap_or(0));
        }

        let mut conn = self.pg_pool.get()?;
        let videos = query.load::<VideoWithOtherData>(conn.deref_mut())?;

        Ok(videos)
    }

    fn delete_video(&self, video_id_value: Uuid) -> Result<(), DatabaseError> {
        let time_now = chrono::offset::Utc::now();
        let mut conn = self.pg_pool.get()?;
        conn.deref_mut().transaction(|conn| {
            let rows_affected = diesel::update(schema::video::table)
                .filter(id.eq(video_id_value))
                .filter(crate::schema::video::deleted_at.is_null())
                .set(crate::schema::video::deleted_at.eq(time_now))
                .execute(conn)?;

            if rows_affected == 0 {
                return Err(diesel::NotFound);
            }

            diesel::update(schema::comment::table)
                .filter(schema::comment::video_id.eq(video_id_value))
                .filter(schema::comment::deleted_at.is_null())
                .set(schema::comment::deleted_at.eq(time_now))
                .execute(conn)?;

            delete(schema::rating::table)
                .filter(schema::rating::video_id.eq(video_id_value))
                .execute(conn)?;

            Ok(())
        })?;

        Ok(())
    }

    fn create_video(&self, new_video: NewVideo) -> Result<Video, DatabaseError> {
        let insert_video: InternalNewVideo = new_video.clone().into();
        let mut conn = self.pg_pool.get()?;
        let video = conn.deref_mut().transaction(|conn| {
            let video = diesel::insert_into(schema::video::table)
                .values(&insert_video)
                .returning(Video::as_returning())
                .get_result(conn)?;

            let video_categories = new_video
                .categories
                .into_iter()
                .map(|ct| VideoCategory {
                    video_id: video.id,
                    category: ct,
                })
                .collect::<Vec<VideoCategory>>();

            diesel::insert_into(schema::video_categories::table)
                .values(&video_categories)
                .execute(conn)?;

            Ok::<Video, DatabaseError>(video)
        })?;

        Ok(video)
    }

    fn edit_video(
        &self,
        video_id: Uuid,
        patch_video: PartialVideo,
    ) -> Result<Video, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let video = diesel::update(schema::video::table)
            .filter(crate::schema::video::id.eq(video_id))
            .filter(crate::schema::video::deleted_at.is_null())
            .set(patch_video)
            .returning(Video::as_returning())
            .get_result(conn.deref_mut())?;

        Ok(video)
    }

    fn increment_view_count(&self, video_id: Uuid) -> Result<(), DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        diesel::update(schema::video::table.filter(schema::video::id.eq(video_id)))
            .set(schema::video::views.eq(schema::video::views + 1))
            .returning(Video::as_returning())
            .execute(conn.deref_mut())?;

        Ok(())
    }
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, ToSchema)]
#[diesel(table_name = crate::schema::video)]
struct InternalNewVideo {
    pub author_id: Uuid,
    pub title: String,
    pub description: String,
    pub url: String,
    pub length: i32,
    pub is_premium: bool,
}

impl From<NewVideo> for InternalNewVideo {
    fn from(internal: NewVideo) -> Self {
        InternalNewVideo {
            author_id: internal.author_id,
            title: internal.title,
            description: internal.description,
            url: internal.url,
            length: internal.length,
            is_premium: internal.is_premium,
        }
    }
}
