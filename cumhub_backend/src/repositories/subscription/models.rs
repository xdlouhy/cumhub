use diesel::{AsChangeset, Insertable};
use serde::Deserialize;
use utoipa::{IntoParams, ToSchema};
use uuid::Uuid;

#[derive(AsChangeset, Insertable, Deserialize, ToSchema, IntoParams)]
#[diesel(table_name = crate::schema::subscription)]
pub struct SubscriptionId {
    pub subscriber_id: Uuid,
    pub author_id: Uuid,
}
