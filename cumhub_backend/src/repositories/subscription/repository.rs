use std::ops::DerefMut;

use diesel::{BoolExpressionMethods, Connection, delete, ExpressionMethods, QueryDsl, RunQueryDsl, SelectableHelper};
use diesel::pg::*;
use diesel::r2d2::{ConnectionManager, Pool};
use uuid::Uuid;

use crate::errors::DatabaseError;
use crate::models::{Subscription, User};
use crate::repositories::subscription::models::SubscriptionId;
use crate::schema;
use crate::schema::subscription::{author_id, subscriber_id};

pub trait SubscriptionRepository {
    fn get_subscriptions(&self, user_id: Uuid) -> Result<Vec<Subscription>, DatabaseError>;
    fn create_subscription(&self, new_subscription: SubscriptionId) -> Result<Subscription, DatabaseError>;
    fn delete_subscription(&self, old_subscription: SubscriptionId) -> Result<(), DatabaseError>;
}

#[derive(Clone)]
pub struct PgSubscriptionRepository {
    pg_pool: Pool<ConnectionManager<PgConnection>>,
}

impl PgSubscriptionRepository {
    pub fn new(pg_pool: Pool<ConnectionManager<PgConnection>>) -> Self {
        Self { pg_pool }
    }
}

impl SubscriptionRepository for PgSubscriptionRepository {
    fn get_subscriptions(&self, user_id_value: Uuid) -> Result<Vec<Subscription>, DatabaseError> {
        let mut conn = self.pg_pool.get()?;

        let subscriptions = schema::subscription::dsl::subscription
            .filter(schema::subscription::dsl::subscriber_id.eq(user_id_value))
            .order(schema::subscription::dsl::created_at.desc())
            .get_results(conn.deref_mut())?;

        Ok(subscriptions)
    }

    fn create_subscription(&self, new_subscription: SubscriptionId) -> Result<Subscription, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let subscription = conn.deref_mut().transaction(|conn| {
            schema::user::dsl::user
                .find(new_subscription.author_id)
                .filter(schema::user::deleted_at.is_null())
                .select(User::as_select())
                .first(conn)?;

            schema::user::dsl::user
                .find(new_subscription.subscriber_id)
                .filter(schema::user::deleted_at.is_null())
                .select(User::as_select())
                .first(conn)?;

            diesel::insert_into(schema::subscription::table)
                .values(&new_subscription)
                .on_conflict((author_id, subscriber_id))
                .do_update()
                .set(&new_subscription)
                .returning(Subscription::as_returning())
                .get_result(conn)
        });

        if let Err(diesel::result::Error::NotFound) = subscription {
            return Err(DatabaseError::BrokenConstraint);
        }

        Ok(subscription?)
    }

    fn delete_subscription(&self, old_subscription: SubscriptionId) -> Result<(), DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let rows_affected = delete(schema::subscription::table)
            .filter(subscriber_id.eq(old_subscription.subscriber_id).and(author_id.eq(old_subscription.author_id)))
            .execute(conn.deref_mut())?;

        if rows_affected == 0 {
            return Err(DatabaseError::NotFound);
        }

        Ok(())
    }
}
