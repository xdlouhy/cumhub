use std::ops::DerefMut;

use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;
use diesel::{delete, PgConnection};
use r2d2::Pool;
use uuid::Uuid;

use crate::errors::DatabaseError;
use crate::models::User;
use crate::repositories::user::models::{NewUser, PartialUser};
use crate::schema;
use crate::schema::rating::author_id;
use crate::schema::user::{deleted_at, email, id, username};

pub trait UserRepository {
    fn get_user(&self, user_id: Uuid) -> Result<User, DatabaseError>;
    fn get_user_by_username(&self, username_filter: String) -> Result<User, DatabaseError>;
    fn get_user_by_email(&self, email_filter: String) -> Result<User, DatabaseError>;
    fn create_user(&self, new_user: NewUser) -> Result<User, DatabaseError>;
    fn edit_user(&self, user_id: Uuid, patch_user: PartialUser) -> Result<User, DatabaseError>;
    fn delete_user(&self, user_id: Uuid) -> Result<(), DatabaseError>;
}

#[derive(Clone)]
pub struct PgUserRepository {
    pg_pool: Pool<ConnectionManager<PgConnection>>,
}

impl PgUserRepository {
    pub fn new(pg_pool: Pool<ConnectionManager<PgConnection>>) -> Self {
        Self { pg_pool }
    }
}

impl UserRepository for PgUserRepository {
    fn get_user(&self, user_id: Uuid) -> Result<User, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let user = schema::user::dsl::user
            .find(user_id)
            .filter(deleted_at.is_null())
            .select(User::as_select())
            .first(conn.deref_mut())?;

        Ok(user)
    }

    fn get_user_by_username(&self, username_filter: String) -> Result<User, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let user = schema::user::dsl::user
            .filter(username.eq(username_filter))
            .filter(deleted_at.is_null())
            .select(User::as_select())
            .first(conn.deref_mut())?;

        Ok(user)
    }

    fn get_user_by_email(&self, email_filter: String) -> Result<User, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let user = schema::user::dsl::user
            .filter(email.eq(email_filter))
            .filter(deleted_at.is_null())
            .select(User::as_select())
            .first(conn.deref_mut())?;

        Ok(user)
    }

    fn create_user(&self, new_user: NewUser) -> Result<User, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let user = diesel::insert_into(schema::user::table)
            .values(&new_user)
            .returning(User::as_returning())
            .get_result(conn.deref_mut())?;

        Ok(user)
    }

    fn edit_user(&self, user_id: Uuid, patch_user: PartialUser) -> Result<User, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let user = diesel::update(schema::user::table)
            .filter(id.eq(user_id))
            .filter(deleted_at.is_null())
            .set(patch_user)
            .returning(User::as_returning())
            .get_result(conn.deref_mut())?;

        Ok(user)
    }

    fn delete_user(&self, user_id: Uuid) -> Result<(), DatabaseError> {
        let time_now = chrono::offset::Utc::now();
        let mut conn = self.pg_pool.get()?;
        conn.deref_mut().transaction(|conn| {
            let rows_affected = diesel::update(schema::user::table)
                .filter(id.eq(user_id))
                .filter(deleted_at.is_null())
                .set((
                    deleted_at.eq(time_now),
                    username.eq(None::<String>),
                    email.eq(None::<String>),
                ))
                .execute(conn)?;

            if rows_affected == 0 {
                return Err(diesel::NotFound);
            }

            diesel::update(schema::video::table)
                .filter(schema::video::author_id.eq(user_id))
                .filter(schema::video::deleted_at.is_null())
                .set(schema::video::deleted_at.eq(time_now))
                .execute(conn)?;

            diesel::update(schema::comment::table)
                .filter(schema::comment::author_id.eq(user_id))
                .filter(schema::comment::deleted_at.is_null())
                .set(schema::comment::deleted_at.eq(time_now))
                .execute(conn)?;

            delete(schema::subscription::table)
                .filter(
                    schema::subscription::subscriber_id
                        .eq(user_id)
                        .or(schema::subscription::author_id.eq(user_id)),
                )
                .execute(conn)?;

            delete(schema::rating::table)
                .filter(author_id.eq(user_id))
                .execute(conn)?;
            Ok(())
        })?;

        Ok(())
    }
}
