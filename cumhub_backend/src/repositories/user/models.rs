use chrono::{DateTime, Utc};
use diesel::{AsChangeset, Insertable};
use serde::Deserialize;
use utoipa::{IntoParams, ToSchema};

use crate::models::User;

#[derive(AsChangeset, Insertable, Deserialize, ToSchema, IntoParams)]
#[diesel(table_name = crate::schema::user)]
pub struct NewUser {
    pub username: String,
    pub email: String,
    pub password_hash: String,
    pub profile_pic_url: String,
    pub description: String,
}

#[derive(AsChangeset, Insertable, Deserialize, ToSchema, Debug)]
#[diesel(table_name = crate::schema::user)]
pub struct PartialUser {
    pub username: Option<String>,
    pub email: Option<String>,
    pub premium_expiration: Option<DateTime<Utc>>,
    pub password_hash: Option<String>,
    pub profile_pic_url: Option<String>,
    pub description: Option<String>,
}

impl From<NewUser> for PartialUser {
    fn from(value: NewUser) -> Self {
        Self {
            username: Some(value.username),
            email: Some(value.email),
            password_hash: Some(value.password_hash),
            premium_expiration: None,
            profile_pic_url: None,
            description: Some(value.description),
        }
    }
}

impl From<User> for PartialUser {
    fn from(value: User) -> Self {
        Self {
            username: value.username,
            email: value.email,
            password_hash: Some(value.password_hash),
            premium_expiration: value.premium_expiration,
            profile_pic_url: value.profile_pic_url,
            description: value.description,
        }
    }
}

impl PartialUser {
    pub fn edit_password_hash(password_hash: String) -> Self {
        Self {
            username: None,
            email: None,
            password_hash: Some(password_hash),
            premium_expiration: None,
            profile_pic_url: None,
            description: None,
        }
    }
}

