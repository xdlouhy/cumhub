use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};
use crate::repositories::common::Pagination;

#[derive(Serialize, Deserialize, Debug, ToSchema, IntoParams, Default, Clone)]
pub struct SelectChannelsManyFilter {
    pub search: Option<String>,
    pub pagination: Option<Pagination>,
}

impl SelectChannelsManyFilter {
    pub fn new(search: Option<String>) -> Self {
        Self {
            search,
            pagination: None,
        }
    }
}