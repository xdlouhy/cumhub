use std::ops::DerefMut;

use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl, SelectableHelper};
use diesel::pg::*;
use diesel::r2d2::{ConnectionManager, Pool};
use uuid::Uuid;

use crate::errors::DatabaseError;
use crate::models::{ChannelInfo};
use crate::repositories::channel::models::SelectChannelsManyFilter;
use crate::schema;
use crate::schema::channel_info::username;

pub trait ChannelsRepository {
    fn get_channel(&self, channel_id: Uuid) -> Result<ChannelInfo, DatabaseError>;
    fn get_channels(&self, filters: SelectChannelsManyFilter) -> Result<Vec<ChannelInfo>, DatabaseError>;
}

#[derive(Clone)]
pub struct PgChannelRepository {
    pg_pool: Pool<ConnectionManager<PgConnection>>,
}

impl PgChannelRepository {
    pub fn new(pg_pool: Pool<ConnectionManager<PgConnection>>) -> Self {
        Self { pg_pool }
    }
}

impl ChannelsRepository for PgChannelRepository {
    fn get_channel(&self, channel_id: Uuid) -> Result<ChannelInfo, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let channel = schema::channel_info::dsl::channel_info
            .find(channel_id)
            .select(ChannelInfo::as_select())
            .first(conn.deref_mut())?;

        Ok(channel)
    }

    fn get_channels(&self, filters: SelectChannelsManyFilter) -> Result<Vec<ChannelInfo>, DatabaseError> {
        let mut query = schema::channel_info::dsl::channel_info
            .into_boxed();

        if let Some(username_filter) = filters.search {
            query = query.filter(username.eq(username_filter));
        }

        if let Some(pagination) = filters.pagination {
            query = query.limit(pagination.limit.unwrap_or(i64::MAX));
            query = query.offset(pagination.offset.unwrap_or(0));
        }

        let mut conn = self.pg_pool.get()?;
        let channels = query.load::<ChannelInfo>(conn.deref_mut())?;

        Ok(channels)
    }
}
