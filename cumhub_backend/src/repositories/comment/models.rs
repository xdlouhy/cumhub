use diesel::{AsChangeset, Insertable};
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};
use uuid::Uuid;

use crate::repositories::common::Pagination;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, ToSchema)]
#[diesel(table_name = crate::schema::comment)]
pub struct NewComment {
    pub video_id: Uuid,
    pub author_id: Uuid,
    pub content: String,
}

#[derive(Serialize, Deserialize, AsChangeset, Insertable, ToSchema)]
#[diesel(table_name = crate::schema::comment)]
pub struct PartialComment {
    pub content: String,
}

#[derive(Serialize, Deserialize, Debug, IntoParams)]
pub struct SelectCommentManyFilter {
    pub video_id: Option<Uuid>,
    pub author_id: Option<Uuid>,
    pub pagination: Option<Pagination>,
}