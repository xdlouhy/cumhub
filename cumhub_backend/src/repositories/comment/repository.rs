use std::ops::DerefMut;

use diesel::{Connection, ExpressionMethods, QueryDsl, RunQueryDsl, SelectableHelper};
use diesel::pg::*;
use diesel::r2d2::{ConnectionManager, Pool};
use uuid::Uuid;

use crate::errors::DatabaseError;
use crate::models::{Comment, CommentWithOtherData, User, Video};
use crate::repositories::comment::models::{NewComment, PartialComment, SelectCommentManyFilter};
use crate::schema;
use crate::schema::comment::deleted_at;
use crate::schema::comment::id;
use crate::schema::comment_details_view::{author_id, video_id};

pub trait CommentRepository {
    fn get_comment(&self, comment_id: Uuid) -> Result<CommentWithOtherData, DatabaseError>;
    fn get_comments(&self, filters: SelectCommentManyFilter) -> Result<Vec<CommentWithOtherData>, DatabaseError>;
    fn create_comment(&self, new_comment: NewComment) -> Result<Comment, DatabaseError>;
    fn edit_comment(&self, comment_id: Uuid, patch_comment: PartialComment) -> Result<Comment, DatabaseError>;

    fn delete_comment(&self, comment_id: Uuid) -> Result<(), DatabaseError>;
}

#[derive(Clone)]
pub struct PgCommentRepository {
    pg_pool: Pool<ConnectionManager<PgConnection>>,
}

impl PgCommentRepository {
    pub fn new(pg_pool: Pool<ConnectionManager<PgConnection>>) -> Self {
        Self { pg_pool }
    }
}

impl CommentRepository for PgCommentRepository {
    fn get_comment(&self, comment_id: Uuid) -> Result<CommentWithOtherData, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let comment = schema::comment_details_view::dsl::comment_details_view
            .find(comment_id)
            .filter(schema::comment_details_view::deleted_at.is_null())
            .select(CommentWithOtherData::as_select())
            .first(conn.deref_mut())?;

        Ok(comment)
    }

    fn get_comments(&self, filters: SelectCommentManyFilter) -> Result<Vec<CommentWithOtherData>, DatabaseError> {
        let mut query = schema::comment_details_view::dsl::comment_details_view
            .filter(schema::comment_details_view::deleted_at.is_null())
            .order(schema::comment_details_view::dsl::created_at.desc())
            .into_boxed();

        if let Some(video_filter) = filters.video_id {
            query = query.filter(video_id.eq(video_filter));
        }

        if let Some(user_uuid) = filters.author_id {
            query = query.filter(author_id.eq(user_uuid));
        }

        if let Some(pagination) = filters.pagination {
            query = query.limit(pagination.limit.unwrap_or(i64::MAX));
            query = query.offset(pagination.offset.unwrap_or(0));
        }

        let mut conn = self.pg_pool.get()?;
        let users = query.load::<CommentWithOtherData>(conn.deref_mut())?;

        Ok(users)
    }

    fn create_comment(&self, new_comment: NewComment) -> Result<Comment, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let comment = conn.deref_mut().transaction(|conn| {
            schema::video::dsl::video
                .find(new_comment.video_id)
                .filter(schema::video::deleted_at.is_null())
                .select(Video::as_select())
                .first(conn)?;

            schema::user::dsl::user
                .find(new_comment.author_id)
                .filter(schema::user::deleted_at.is_null())
                .select(User::as_select())
                .first(conn)?;

            diesel::insert_into(schema::comment::table)
                .values(&new_comment)
                .returning(Comment::as_returning())
                .get_result(conn)
        });

        if let Err(diesel::result::Error::NotFound) = comment {
            return Err(DatabaseError::BrokenConstraint);
        }

        Ok(comment?)
    }

    fn edit_comment(&self, comment_id: Uuid, patch_comment: PartialComment) -> Result<Comment, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let comment = diesel::update(schema::comment::table)
            .filter(id.eq(comment_id))
            .filter(deleted_at.is_null())
            .set(patch_comment)
            .returning(Comment::as_returning())
            .get_result(conn.deref_mut())?;

        Ok(comment)
    }

    fn delete_comment(&self, comment_id: Uuid) -> Result<(), DatabaseError> {
        let time_now = chrono::offset::Utc::now();
        let mut conn = self.pg_pool.get()?;
        let rows_affected = diesel::update(schema::comment::table)
            .filter(id.eq(comment_id))
            .filter(deleted_at.is_null())
            .set(deleted_at.eq(time_now))
            .execute(conn.deref_mut())?;

        if rows_affected == 0 {
            return Err(DatabaseError::NotFound);
        }

        Ok(())
    }
}
