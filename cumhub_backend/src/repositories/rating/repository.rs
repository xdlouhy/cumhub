use std::ops::DerefMut;

use diesel::result::Error;
use diesel::{BoolExpressionMethods, Connection, delete, ExpressionMethods, QueryDsl, RunQueryDsl, SelectableHelper};
use diesel::pg::*;
use diesel::r2d2::{ConnectionManager, Pool};
use uuid::Uuid;

use crate::errors::DatabaseError;
use crate::models::{Rating, User, Video};
use crate::repositories::rating::models::{NewRating, RatingId, SelectRatingsManyFilter};
use crate::schema;
use crate::schema::rating::{author_id, liked, video_id};

pub trait RatingRepository {
    fn get_ratings(&self, filters: SelectRatingsManyFilter) -> Result<Vec<Rating>, DatabaseError>;
    fn get_rating(&self, video_id: Uuid, author_id: Uuid) -> Result<Option<Rating>, DatabaseError>;
    fn create_rating(&self, new_rating: NewRating) -> Result<Rating, DatabaseError>;
    fn delete_rating(&self, rating_id: RatingId) -> Result<(), DatabaseError>;
}

#[derive(Clone)]
pub struct PgRatingRepository {
    pg_pool: Pool<ConnectionManager<PgConnection>>,
}

impl PgRatingRepository {
    pub fn new(pg_pool: Pool<ConnectionManager<PgConnection>>) -> Self {
        Self { pg_pool }
    }
}

impl RatingRepository for PgRatingRepository {
    fn get_ratings(&self, filters: SelectRatingsManyFilter) -> Result<Vec<Rating>, DatabaseError> {
        let mut query = schema::rating::dsl::rating
            .order(schema::rating::dsl::created_at.desc())
            .into_boxed();

        if let Some(filter_user) = filters.author_id {
            query = query.filter(author_id.eq(filter_user));
        }

        if let Some(filter_video) = filters.video_id {
            query = query.filter(video_id.eq(filter_video));
        }

        if let Some(filter_liked) = filters.liked {
            query = query.filter(liked.eq(filter_liked));
        }

        if let Some(pagination) = filters.pagination {
            query = query.limit(pagination.limit.unwrap_or(i64::MAX));
            query = query.offset(pagination.offset.unwrap_or(0));
        }

        let mut conn = self.pg_pool.get()?;
        let ratings = query.load::<Rating>(conn.deref_mut())?;

        Ok(ratings)
    }

    fn get_rating(&self, vid_id: Uuid, aut_id: Uuid) -> Result<Option<Rating>, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let result = schema::rating::dsl::rating
            .filter(schema::rating::dsl::author_id.eq(aut_id))
            .filter(schema::rating::dsl::video_id.eq(vid_id))
            .select(Rating::as_select())
            .first::<Rating>(conn.deref_mut());
    
        match result {
            Ok(rating) => Ok(Some(rating)),
            Err(Error::NotFound) => Ok(None), // No rating found
            Err(e) => Err(e.into()), // Other database errors
        }
    }

    fn create_rating(&self, new_rating: NewRating) -> Result<Rating, DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let rating = conn.deref_mut().transaction(|conn| {
            schema::video::dsl::video
                .find(new_rating.video_id)
                .filter(schema::video::deleted_at.is_null())
                .select(Video::as_select())
                .first(conn)?;

            schema::user::dsl::user
                .find(new_rating.author_id)
                .filter(schema::user::deleted_at.is_null())
                .select(User::as_select())
                .first(conn)?;

            diesel::insert_into(schema::rating::table)
                .values(&new_rating)
                .on_conflict((author_id, video_id))
                .do_update()
                .set(&new_rating)
                .returning(Rating::as_returning())
                .get_result(conn)
        });

        if let Err(diesel::result::Error::NotFound) = rating {
            return Err(DatabaseError::BrokenConstraint);
        }

        Ok(rating?)
    }

    fn delete_rating(&self, rating_id: RatingId) -> Result<(), DatabaseError> {
        let mut conn = self.pg_pool.get()?;
        let rows_affected = delete(schema::rating::table)
            .filter(
                author_id
                    .eq(rating_id.author_id)
                    .and(video_id.eq(rating_id.video_id)),
            )
            .execute(conn.deref_mut())?;

        if rows_affected == 0 {
            return Err(DatabaseError::NotFound);
        }

        Ok(())
    }
}
