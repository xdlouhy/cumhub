use diesel::{AsChangeset, Insertable};
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};
use uuid::Uuid;

use crate::repositories::common::Pagination;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, ToSchema)]
#[diesel(table_name = crate::schema::rating)]
pub struct NewRating {
    pub video_id: Uuid,
    pub author_id: Uuid,
    pub liked: bool
}

#[derive(Serialize, Deserialize, Debug, IntoParams)]
pub struct SelectRatingsManyFilter {
    pub video_id: Option<Uuid>,
    pub author_id: Option<Uuid>,
    pub liked: Option<bool>,
    pub pagination: Option<Pagination>,
}

#[derive(Serialize, Deserialize, Debug, ToSchema, IntoParams)]
pub struct RatingId {
    pub video_id: Uuid,
    pub author_id: Uuid,
}