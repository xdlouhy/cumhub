use askama::Template;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::models::{ChannelInfo, CommentWithOtherData, Rating, User, VideoWithOtherData};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserInfo {
    pub id: Uuid,
    pub username: String,
    pub email: String,
    pub description: Option<String>,
    pub profile_picture: Option<String>,
    pub premium_expiration: Option<DateTime<Utc>>,
    pub has_premium: bool,
}

impl From<User> for UserInfo {
    fn from(user: User) -> Self {
        let has_premium = match user.premium_expiration {
            Some(date) => Utc::now().lt(&date),
            None => false,
        };
        UserInfo {
            id: user.id,
            username: user.username.unwrap_or_default(),
            email: user.email.unwrap_or_default(),
            description: user.description,
            profile_picture: user.profile_pic_url,
            premium_expiration: user.premium_expiration,
            has_premium,
        }
    }
}

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate {
    pub already_logged_in: bool,
    pub categories: Vec<String>,
    pub category: String,
    pub user_info: Option<UserInfo>,
    pub title_substring: String,
    pub sort_by: String,
    pub videos: Vec<VideoWithOtherData>,
}

#[derive(Template)]
#[template(path = "profile_page.html")]
pub struct ProfileTemplate {
    pub already_logged_in: bool,
    pub categories: Vec<String>,
    pub profile_name: String,
    pub profile_description: String,
    pub subscribers: u32,
    pub user_subscribed: bool,
    pub videos: Vec<VideoWithOtherData>,
    pub search: String,
    pub sort_by: String,
    pub user_info: Option<UserInfo>,
    pub channel_pic_url: Option<String>,
}

#[derive(Template)]
#[template(path = "./common/video.html")]
pub struct VideoTemplate {
    pub video: VideoWithOtherData,
}

#[derive(Template)]
#[template(path = "./common/video_grid.html")]
pub struct VideosTemplate {
    pub videos: Vec<VideoWithOtherData>,
}

#[derive(Template)]
#[template(path = "./common/comment.html")]
pub struct CommentTemplate {
    pub comment: CommentWithOtherData,
}

#[derive(Template)]
#[template(path = "./common/comments.html")]
pub struct CommentsTemplate {
    pub comments: Vec<CommentWithOtherData>,
}

#[derive(Template)]
#[template(path = "./video_page.html")]
pub struct VideoPageTemplate {
    pub video: VideoWithOtherData,
    pub channel_info: ChannelInfo,
    pub comments: Vec<CommentWithOtherData>,
    pub user_info: Option<UserInfo>,
    pub user_rating: Option<Rating>,
    pub categories: Vec<String>,
    pub already_logged_in: bool,
    pub already_liked: Option<bool>,
    pub already_disliked: Option<bool>,
}

#[derive(Template)]
#[template(path = "login_page.html")]
pub struct LoginTemplate {
    pub already_logged_in: bool,
    pub login_error_message: Option<String>,
}

#[derive(Template)]
#[template(path = "./login_page/page_content.html")]
pub struct LoginPageContentTemplate {
    pub already_logged_in: bool,
    pub login_error_message: Option<String>,
}

#[derive(Template)]
#[template(path = "upload_page.html")]
pub struct UploadTemplate {
    pub uploaded: bool,
}

#[derive(Template)]
#[template(path = "access_denied.html")]
pub struct AccessDeniedTemplate {}

#[derive(Template)]
#[template(path = "not_found.html")]
pub struct NotFoundTemplate {
    pub categories: Vec<String>,
    pub user_info: Option<UserInfo>,
}

#[derive(Template)]
#[template(path = "bad_request.html")]
pub struct BadRequestTemplate {
    pub message: String,
    pub categories: Vec<String>,
    pub user_info: Option<UserInfo>,
}

#[derive(Template)]
#[template(path = "./upload_page/upload_successful.html")]
pub struct UploadSuccessfulTemplate {
    pub recorded_video: String,
}

#[derive(Template)]
#[template(path = "register_page.html")]
pub struct RegisterTemplate {
    pub already_logged_in: bool,
}

#[derive(Template)]
#[template(path = "account_settings_page.html")]
pub struct AccountSettingsTemplate {
    pub user_id: String,
    pub description: String,
    pub profile_picture: String,
    pub username: String,

    pub username_message: Option<String>,
    pub password_message: Option<String>,
    pub profile_pic_message: Option<String>,
    pub description_message: Option<String>,

    pub username_success: bool,
    pub password_success: bool,
    pub profile_pic_success: bool,
    pub description_success: bool,
}

#[derive(Template)]
#[template(path = "subscribed_page.html")]
pub struct SubscribedTemplate {
    pub already_logged_in: bool,
    pub channels: Vec<ChannelInfo>,
    pub channel_search: String,
    pub videos: Vec<VideoWithOtherData>,
    pub title_substring: String,
    pub sort_by: String,
    pub user_info: Option<UserInfo>,
    pub categories: Vec<String>,
}

#[derive(Template)]
#[template(path = "./subscribed_page/channel/channel_list.html")]
pub struct ChannelsListTemplate {
    pub channels: Vec<ChannelInfo>,
    pub channel_search: String,
}

#[derive(Template)]
#[template(path = "./subscribed_page/video/videos.html")]
pub struct ChannelsVideosTemplate {
    pub title_substring: String,
    pub sort_by: String,
    pub videos: Vec<VideoWithOtherData>,
}

#[derive(Template)]
#[template(path = "premium_page.html")]
pub struct PremiumTemplate {
    pub user_info: Option<UserInfo>,
}

#[derive(Template)]
#[template(path = "./premium_page/subscribed_successfully.html")]
pub struct PremiumSubscribedSuccessfullyTemplate {}

#[derive(Template)]
#[template(path = "logout_confirm_template.html")]
pub struct LogoutConfirmTemplate {}

#[derive(Template)]
#[template(path = "./common/hacked_redirect.html")]
pub struct HackedRedirectTemplate {
    pub redirect_url: String,
}

#[derive(Template)]
#[template(path = "./video_page/video_stats.html")]
pub struct VideoStatsTemplate {
    pub video: VideoWithOtherData,
    pub already_logged_in: bool,
    pub already_liked: Option<bool>,
    pub already_disliked: Option<bool>,
}
