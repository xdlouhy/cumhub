-- This file should undo anything in `up.sql`
DROP VIEW IF EXISTS "channel_info";
DROP VIEW IF EXISTS "comment_details_view";
DROP TABLE IF EXISTS "comment";
DROP VIEW IF EXISTS "video_details_view";

DROP TABLE IF EXISTS "rating";

DROP TABLE IF EXISTS "video_categories";
DROP TABLE IF EXISTS "video";
DROP TYPE IF EXISTS category;

DROP TABLE IF EXISTS "subscription";
DROP TABLE IF EXISTS "user";