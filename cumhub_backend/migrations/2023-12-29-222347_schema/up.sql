-- Your SQL goes here
CREATE TABLE "user" (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    username VARCHAR(255) UNIQUE,
    email VARCHAR(255) UNIQUE,
    description VARCHAR(255),
    profile_pic_url VARCHAR(255),
    premium_expiration TIMESTAMPTZ,
    password_hash VARCHAR(255) NOT NULL,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at TIMESTAMPTZ
);

-- SUBSCRIPTIONS

CREATE TABLE "subscription" (
    subscriber_id UUID NOT NULL REFERENCES "user"(id),
    author_id UUID NOT NULL REFERENCES "user"(id),
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (subscriber_id, author_id)
);

-- VIDEO

CREATE TYPE category AS ENUM ('VANILLA', 'BDSM', 'LESBIAN', 'GAY', 'FETISH', 'FEMDOM', 'ANAL', 'ORAL', 'GROUP', 'SOLO');

CREATE TABLE "video" (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    author_id UUID NOT NULL REFERENCES "user"(id),
    title VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    url VARCHAR(255) NOT NULL,
    views INT NOT NULL DEFAULT 0,
    length INT NOT NULL,
    is_premium BOOLEAN NOT NULL DEFAULT false,
    uploaded_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at TIMESTAMPTZ
);

CREATE TABLE "video_categories" (
    video_id UUID NOT NULL REFERENCES video(id),
    category category NOT NULL,
    PRIMARY KEY (video_id, category)
);

-- Rating

CREATE TABLE "rating" (
                          author_id UUID NOT NULL REFERENCES "user"(id),
                          video_id UUID NOT NULL REFERENCES "video"(id),
                          liked BOOLEAN NOT NULL,
                          created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
                          PRIMARY KEY (author_id, video_id)
);

CREATE VIEW "video_details_view" AS
SELECT
    vid.id,
    vid.author_id,
    u.username as author_username,
    vid.title,
    vid.description,
    vid.url,
    vid.views,
    vid.length,
    vid.is_premium,
    vid.uploaded_at,
    vid.deleted_at,
    vid.categories,
    COALESCE(liked.liked_sum, 0) as likes
FROM (
         SELECT
             v.id,
             v.author_id,
             v.title,
             v.description,
             v.url,
             v.views,
             v.length,
             v.is_premium,
             v.uploaded_at,
             v.deleted_at,
             ARRAY_REMOVE(ARRAY_AGG(vc.category), NULL) AS categories
         FROM
             video v
                 FULL JOIN video_categories vc ON v.id = vc.video_id
         GROUP BY
             v.id) as vid
         INNER JOIN "user" u ON u.id=vid.author_id
         LEFT JOIN (
    SELECT
        video_id,
        SUM(CASE WHEN liked THEN 1 ELSE -1 END) AS liked_sum
    FROM rating
    GROUP BY rating.video_id) as liked ON vid.id = liked.video_id;

CREATE TABLE "comment" (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    author_id UUID NOT NULL REFERENCES "user"(id),
    video_id UUID NOT NULL REFERENCES "video"(id),
    content VARCHAR(255) NOT NULL,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at TIMESTAMPTZ
);

CREATE VIEW "comment_details_view" AS
SELECT
    c.id,
    c.author_id,
    u.username as author_username,
    c.video_id,
    c.content,
    c.created_at,
    c.deleted_at

FROM "comment" as c
         INNER JOIN "user" u ON u.id=c.author_id
WHERE u.deleted_at IS NULL;


CREATE VIEW "channel_info" AS
SELECT
    u.id,
    u.username,
    u.description,
    u.profile_pic_url as profile_picture,
    COALESCE(s.subs, 0) as subscribers
FROM "user" as u
LEFT JOIN (
    SELECT
        author_id,
        COUNT(subscriber_id) as subs
    FROM "subscription"
    GROUP BY author_id) as s
ON u.id = s.author_id
WHERE u.deleted_at IS NULL;